import { sveltekit } from "@sveltejs/kit/vite"
import morgan from "morgan"
import Icons from "unplugin-icons/vite"
import { defineConfig } from "vitest/config"

const middlewaresPlugin = {
  name: "middlewares",
  async configureServer(server) {
    server.middlewares.use(morgan("dev"))
  },
}

export default defineConfig({
  plugins: [
    sveltekit(),
    Icons({
      compiler: "svelte",
    }),
    middlewaresPlugin,
  ],
  test: {
    include: ["src/**/*.{test,spec}.{js,ts}"],
  },
})
