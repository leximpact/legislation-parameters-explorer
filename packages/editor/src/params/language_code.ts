import type { ParamMatcher } from "@sveltejs/kit"

import { allLanguageCodes, type LanguageCode } from "$lib/i18n"

export const match: ParamMatcher = (param) =>
  allLanguageCodes.includes(param as LanguageCode)
