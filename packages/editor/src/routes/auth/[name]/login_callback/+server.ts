import { error } from "@sveltejs/kit"
import jwt from "jsonwebtoken"
import { authorizationCodeGrant } from "openid-client"

import config from "$lib/server/config"
import type { SigninPayload } from "$lib/users"

import type { RequestHandler } from "./$types"

export const GET: RequestHandler = async ({ cookies, locals, params, url }) => {
  const { openIdConnectConfigurationByName } = locals
  const openIdConnectConfiguration =
    openIdConnectConfigurationByName?.[params.name]
  if (openIdConnectConfiguration === undefined) {
    console.error(`No authentication method defined`)
    return new Response("No authentication method defined", {
      status: 302,
      headers: { location: "/" },
    })
  }

  const signinJwt = cookies.get("signin")
  cookies.delete("signin", { path: "/" })
  if (signinJwt === undefined) {
    console.warn(`Authentication failed: Missing signin cookie`)
    return new Response(`Authentication failed: Missing signin cookie.`, {
      status: 302,
      headers: { location: "/" },
    })
  }
  let signinPayload: SigninPayload & {
    pkceCodeVerifier: string
    redirectUrl: string
    state?: string
  }
  try {
    signinPayload = jwt.verify(signinJwt, config.jwtSecret) as SigninPayload & {
      pkceCodeVerifier: string
      redirectUrl: string
      state?: string
    }
  } catch (e) {
    console.error(`Invalid JSON Web Token: ${signinJwt}. ${e}`)
    error(401, `Invalid JSON Web Token`)
  }

  const tokens = await authorizationCodeGrant(openIdConnectConfiguration, url, {
    pkceCodeVerifier: signinPayload.pkceCodeVerifier,
    expectedState: signinPayload.state,
  })

  const user = { ...tokens.claims() }
  delete user.exp
  delete user.iat
  user.issuerName = params.name
  user.username = user.preferred_username

  if (tokens.id_token !== undefined) {
    cookies.set(
      "id_token",
      jwt.sign({ id_token: tokens.id_token }, config.jwtSecret, {
        expiresIn: "1w",
      }),
      {
        httpOnly: true,
        path: "/",
        secure: true,
      },
    )
  }
  cookies.set("user", jwt.sign(user, config.jwtSecret, { expiresIn: "1w" }), {
    httpOnly: true,
    path: "/",
    secure: true,
  })

  const redirectUrl = signinPayload.redirectUrl
  return new Response(`Redirecting to ${redirectUrl}…`, {
    status: 302,
    headers: {
      location: redirectUrl,
    },
  })
}
