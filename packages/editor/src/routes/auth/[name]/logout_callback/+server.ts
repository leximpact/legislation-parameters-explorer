import type { RequestHandler } from "./$types"

export const GET: RequestHandler = async () => {
  const redirectUrl = "/"
  return new Response(`Redirecting to ${redirectUrl}…`, {
    status: 302,
    headers: {
      location: redirectUrl,
    },
  })
}
