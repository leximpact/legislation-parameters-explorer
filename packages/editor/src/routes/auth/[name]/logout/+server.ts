import { error } from "@sveltejs/kit"
import { buildEndSessionUrl } from "openid-client"

import { validateLoginLogoutQuery } from "$lib/server/auditors/queries"

import type { RequestHandler } from "./$types"

export const GET: RequestHandler = ({ cookies, locals, params, url }) => {
  const [query, queryError] = validateLoginLogoutQuery(url.origin)(
    url.searchParams,
  ) as [{ redirect: string }, unknown]
  if (queryError !== null) {
    console.error(
      `Error in ${url.pathname} query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
    error(
      400,
      `Invalid query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
  }
  // const { redirect: redirectUrl } = query

  cookies.delete("id_token", { path: "/" })
  cookies.delete("user", { path: "/" })
  const idToken = locals.id_token
  delete locals.id_token
  delete locals.user

  if (idToken === undefined) {
    const redirectUrl = "/"
    return new Response(`Redirecting to ${redirectUrl}…`, {
      status: 302,
      headers: { location: redirectUrl },
    })
  }

  const { openIdConnectConfigurationByName } = locals
  const openIdConnectConfiguration =
    openIdConnectConfigurationByName?.[params.name]
  if (openIdConnectConfiguration !== undefined) {
    const endSessionUrl = buildEndSessionUrl(openIdConnectConfiguration, {
      id_token_hint: idToken,
      post_logout_redirect_uri: new URL(
        `auth/${params.name}/logout_callback`,
        url.origin,
      ).toString(),
    })
    return new Response(`Redirecting to ${endSessionUrl}…`, {
      status: 302,
      headers: { location: endSessionUrl.toString() },
    })
  }

  console.error(`No authentication method defined`)
  return new Response("No authentication method defined", {
    status: 302,
    headers: { location: "/" },
  })
}
