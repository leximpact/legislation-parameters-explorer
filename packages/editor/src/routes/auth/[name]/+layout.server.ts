import config from "$lib/server/config"
import { error } from "@sveltejs/kit"

import type { LayoutServerLoad } from "./$types"

const { openIdConnectByName } = config

export const load: LayoutServerLoad = ({ params }) => {
  const { name } = params
  if (!Object.keys(openIdConnectByName ?? {}).includes(name)) {
    error(404, `Fournisseur d'identité OpenID Connect inconnu : ${name}`)
  }
  return {}
}
