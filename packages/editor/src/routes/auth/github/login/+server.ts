import { error } from "@sveltejs/kit"
import jwt from "jsonwebtoken"
import {
  buildAuthorizationUrl,
  calculatePKCECodeChallenge,
  randomPKCECodeVerifier,
  randomState,
} from "openid-client"

import { validateLoginLogoutQuery } from "$lib/server/auditors/queries"
import config from "$lib/server/config"

import type { RequestHandler } from "./$types"

export const GET: RequestHandler = async ({ cookies, locals, url }) => {
  if (locals.githubConfiguration === undefined) {
    error(404, "GitHub authentication is disabled")
  }

  const [query, queryError] = validateLoginLogoutQuery(url.origin)(
    url.searchParams,
  ) as [{ redirect: string }, unknown]
  if (queryError !== null) {
    console.error(
      `Error in ${url.pathname} query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
    error(
      400,
      `Invalid query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
  }
  const { redirect: redirectUrl } = query

  const { githubConfiguration } = locals
  if (githubConfiguration !== undefined) {
    // PKCE: The following MUST be generated for every redirect to the
    // authorization_endpoint. You must store the codeVerifier and state in the
    // end-user session such that it can be recovered as the user gets redirected
    // from the authorization server back to your application.
    //
    const pkceCodeVerifier: string = randomPKCECodeVerifier()
    const parameters: Record<string, string> = {
      redirect_uri: new URL(
        "auth/github/login_callback",
        url.origin,
      ).toString(),
      scope: "read:user user:email",
      code_challenge: await calculatePKCECodeChallenge(pkceCodeVerifier),
      code_challenge_method: "S256",
    }

    let state: string | undefined = undefined
    if (!githubConfiguration.serverMetadata().supportsPKCE()) {
      // We cannot be sure the server supports PKCE so we're going to use state too.
      // Use of PKCE is backwards compatible even if the AS doesn't support it which
      // is why we're using it regardless. Like PKCE, random state must be generated
      // for every redirect to the authorization_endpoint.
      state = randomState()
      parameters.state = state
    }

    const authorizationUrl: URL = buildAuthorizationUrl(
      githubConfiguration,
      parameters,
    )

    cookies.set(
      "signin",
      jwt.sign({ pkceCodeVerifier, redirectUrl, state }, config.jwtSecret, {
        expiresIn: "1h",
      }),
      {
        httpOnly: true,
        path: "/",
        sameSite: "none", // Same-Site must be set to none, otherwise cookie will not be available.
        secure: true,
      },
    )
    return new Response(`Redirecting to ${authorizationUrl}…`, {
      status: 302,
      headers: { location: authorizationUrl.toString() },
    })
  }

  console.error(`No authentication method defined`)
  return new Response("No authentication method defined", {
    status: 302,
    headers: { location: "/" },
  })
}
