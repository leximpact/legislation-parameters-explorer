import { error } from "@sveltejs/kit"
import jwt from "jsonwebtoken"
import { authorizationCodeGrant } from "openid-client"

import config from "$lib/server/config"
import type { GithubUser, SigninPayload } from "$lib/users"

import type { RequestHandler } from "./$types"

export const GET: RequestHandler = async ({ cookies, locals, url }) => {
  const githubConfiguration = locals.githubConfiguration
  if (githubConfiguration === undefined) {
    error(404, "Github authentication is disabled")
  }

  const signinJwt = cookies.get("signin")
  cookies.delete("signin", { path: "/" })
  if (signinJwt === undefined) {
    console.warn(`Authentication failed: Missing signin cookie`)
    return new Response(`Authentication failed: Missing signin cookie.`, {
      status: 302,
      headers: { location: "/" },
    })
  }
  let signinPayload: SigninPayload & {
    pkceCodeVerifier: string
    redirectUrl: string
    state?: string
  }
  try {
    signinPayload = jwt.verify(signinJwt, config.jwtSecret) as SigninPayload & {
      pkceCodeVerifier: string
      redirectUrl: string
      state?: string
    }
  } catch (e) {
    console.error(`Invalid JSON Web Token: ${signinJwt}. ${e}`)
    error(401, `Invalid JSON Web Token`)
  }

  const tokens = (await authorizationCodeGrant(githubConfiguration, url, {
    pkceCodeVerifier: signinPayload.pkceCodeVerifier,
    expectedState: signinPayload.state,
  })) as {
    access_token: string
    token_type: "bearer"
    scope: "read:user,user:email"
  }
  // console.log("Received tokenSet:", tokenSet)

  const userResponse = await fetch("https://api.github.com/user", {
    headers: {
      Accept: "application/vnd.github+json",
      Authorization: `Bearer ${tokens.access_token}`,
      "X-GitHub-Api-Version": "2022-11-28",
    },
  })
  if (!userResponse.ok) {
    console.error("Retrieval of user GitHub infos failed")
    console.error(`${userResponse.status} ${userResponse.statusText}`)
    console.error(await userResponse.text())
    error(userResponse.status, userResponse.statusText)
  }
  const user = (await userResponse.json()) as GithubUser
  // console.log("Received user infos:", user)
  user.issuerName = "github"
  user.username = user.login

  cookies.set("user", jwt.sign(user, config.jwtSecret, { expiresIn: "1w" }), {
    httpOnly: true,
    path: "/",
    secure: true,
  })

  const redirectUrl = signinPayload.redirectUrl
  return new Response(`Redirecting to ${redirectUrl}…`, {
    status: 302,
    headers: {
      location: redirectUrl,
    },
  })
}
