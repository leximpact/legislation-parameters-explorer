import { error } from "@sveltejs/kit"

import type { RequestHandler } from "./$types"

export const GET: RequestHandler = async ({ locals }) => {
  if (locals.githubConfiguration === undefined) {
    error(404, "Github authentication is disabled")
  }

  const redirectUrl = "/"
  return new Response(`Redirecting to ${redirectUrl}…`, {
    status: 302,
    headers: {
      location: redirectUrl,
    },
  })
}
