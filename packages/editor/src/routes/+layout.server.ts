// import type { Parameter, Unit } from "@tax-benefit/openfisca-json-model"

import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"
import { unitByName, units } from "$lib/server/units"

import type { LayoutServerLoad } from "./$types"

const { openIdConnectByName, parametersRepository, title: appTitle } = config

export const load: LayoutServerLoad = ({ locals }): App.PageData => {
  const publicParametersRepository = { ...parametersRepository }
  delete publicParametersRepository.accessToken
  return {
    appTitle,
    hasRespositoryToken: parametersRepository.accessToken !== undefined,
    openIdConnectTitleByName: Object.fromEntries(
      Object.entries(openIdConnectByName ?? {}).map(([name, { title }]) => [
        name,
        title,
      ]),
    ),
    parametersRepository: publicParametersRepository,
    rootParameter,
    unitByName,
    units,
    user: locals.user,
  }
}
