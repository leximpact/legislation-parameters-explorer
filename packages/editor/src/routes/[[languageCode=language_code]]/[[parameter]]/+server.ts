import { strictAudit } from "@auditors/core"
import {
  ParameterClass,
  yamlFromRawParameter,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"
import { error, json } from "@sveltejs/kit"
import { randomBytes } from "crypto"

import { assertNever } from "$lib/asserts"
import {
  auditParameterEdition,
  convertEditableParameterToRawAndClean,
  getEmbeddedAndFileParameters,
} from "$lib/parameters_edition"
import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"
import { units } from "$lib/server/units"

import type { RequestHandler } from "./$types"

const { github, openIdConnectByName, parametersRepository } = config
const issuerTitleByName = {
  ...Object.fromEntries(
    Object.entries(openIdConnectByName ?? {}).map(([name, { title }]) => [
      name,
      title,
    ]),
  ),
  github: "GitHub",
} as { [name: string]: string }
const issuerUrlByName = {
  ...Object.fromEntries(
    Object.entries(openIdConnectByName ?? {}).map(([name, { issuerUrl }]) => [
      name,
      issuerUrl,
    ]),
  ),
  github: github.issuerUrl,
} as { [name: string]: string }

export const PUT: RequestHandler = async ({
  fetch,
  locals,
  request,
  params,
}) => {
  if (parametersRepository.accessToken === undefined) {
    error(
      403,
      `L'édition de paramètres est impossible car le serveur n'a pas de jeton d'accès au projet https//${parametersRepository.forgeDomainName}/${parametersRepository.group}/${parametersRepository.project}.`,
    )
  }

  const { parameter: name } = params
  const { embeddedParameter, fileParameter, rawFileParameterYaml } =
    await getEmbeddedAndFileParameters(
      rootParameter,
      name,
      parametersRepository,
      units,
    )

  if (fileParameter === undefined || rawFileParameterYaml === undefined) {
    error(
      400,
      `L'édition du paramètre "${name}" est impossible car le paramètre n'a pas de fichier YAML qui lui soit propre.`,
    )
  }

  const { user } = locals
  const username = user?.username

  const [parameterEdition, parameterEditionError] = auditParameterEdition(
    units,
    // Since parameter YAML file (most of the times) doesn't contain the children,
    // inject the children IDs to be able to validate `order` metadata.
    embeddedParameter.class === ParameterClass.Node &&
      embeddedParameter.children !== undefined
      ? Object.keys(embeddedParameter.children)
      : undefined,
    username,
  )(strictAudit, await request.json()) as [
    { email: string; fullname: string; message?: string; parameter: Parameter },
    unknown,
  ]
  if (parameterEditionError !== null) {
    error(400, JSON.stringify(parameterEditionError, null, 2))
  }

  const rawParameter = convertEditableParameterToRawAndClean(
    parameterEdition.parameter,
  )
  const rawParameterYaml = yamlFromRawParameter(rawParameter)
  if (rawFileParameterYaml.trim() === rawParameterYaml.trim()) {
    // Parameter is not modified.
    return json({
      warning:
        "Le paramètre n'a pas été enregistré, car il n'avait aucune modification.",
    })
  }

  const privateNoteBody = [
    "Modification proposée par :",
    user === undefined || username === undefined
      ? undefined
      : `* Identifiant ${issuerTitleByName[user.issuerName]} : [@${username}](${new URL(`/${username}`, issuerUrlByName[user.issuerName])})`,
    `* Nom : ${parameterEdition.fullname}`,
    `* Email : ${parameterEdition.email}`,
    parameterEdition.message === undefined
      ? undefined
      : `Message :\n\n${parameterEdition.message}`,
  ]
    .filter(Boolean)
    .join("\n\n")

  switch (parametersRepository.forgeType) {
    case "GitHub": {
      // Get current SHA of parameter.
      const filePath = embeddedParameter.file_path
      const parameterContentUrl = `https://api.${parametersRepository.forgeDomainName}/repos/${
        parametersRepository.group
      }/${
        parametersRepository.project
      }/contents/${filePath}?ref=${encodeURIComponent(parametersRepository.branch)}`
      const parameterContentResponse = await fetch(parameterContentUrl, {
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${parametersRepository.accessToken!}`,
          "X-GitHub-Api-Version": "2022-11-28",
        },
      })
      if (!parameterContentResponse.ok) {
        console.error(
          `Error while getting SHA of parameter at ${parameterContentUrl}`,
        )
        error(
          parameterContentResponse.status,
          await parameterContentResponse.text(),
        )
      }
      const parameterContent = await parameterContentResponse.json()

      // Get SHA of latest commit of OpenFisca branch.
      const sourceBranchUrl = `https://api.${parametersRepository.forgeDomainName}/repos/${parametersRepository.group}/${parametersRepository.project}/git/refs/heads/${parametersRepository.branch}`
      const sourceBranchResponse = await fetch(sourceBranchUrl, {
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${parametersRepository.accessToken}`,
          "X-GitHub-Api-Version": "2022-11-28",
        },
      })
      if (!sourceBranchResponse.ok) {
        console.error(
          `Error while getting SHA of latest commit at ${sourceBranchUrl}`,
        )
        error(sourceBranchResponse.status, await sourceBranchResponse.text())
      }
      const sourceBranch = await sourceBranchResponse.json()

      // Create new branch.
      const newBranchName = `parameter_${name}_${randomBytes(4).toString("hex")}`
      const newBranchUrl = `https://api.${parametersRepository.forgeDomainName}/repos/${parametersRepository.group}/${parametersRepository.project}/git/refs`
      const newBranchResponse = await fetch(newBranchUrl, {
        body: JSON.stringify(
          { ref: `refs/heads/${newBranchName}`, sha: sourceBranch.object.sha },
          null,
          2,
        ),
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${parametersRepository.accessToken!}`,
          "Content-Type": "application/json",
          "X-GitHub-Api-Version": "2022-11-28",
        },
        method: "POST",
      })
      if (!newBranchResponse.ok) {
        console.error(
          `Error while creating new branch ${newBranchName} at ${newBranchUrl}`,
        )
        error(newBranchResponse.status, await newBranchResponse.text())
      }

      // Update parameter & commit it.
      const parameterUpdateUrl = `https://api.${parametersRepository.forgeDomainName}/repos/${parametersRepository.group}/${parametersRepository.project}/contents/${filePath}`
      const parameterUpdateResponse = await fetch(parameterUpdateUrl, {
        body: JSON.stringify(
          {
            branch: newBranchName,
            commiter: {
              author: {
                email: parametersRepository.authorEmail,
                name: parametersRepository.authorName,
              },
            },
            content: Buffer.from(rawParameterYaml).toString("base64"),
            message: `Modification du paramètre ${name}`,
            sha: parameterContent.sha,
          },
          null,
          2,
        ),
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${parametersRepository.accessToken!}`,
          "Content-Type": "application/json",
          "X-GitHub-Api-Version": "2022-11-28",
        },
        method: "PUT",
      })
      if (!parameterUpdateResponse.ok) {
        console.error(
          `Error while updating parameter & commiting it at ${parameterUpdateUrl}`,
        )
        error(
          parameterUpdateResponse.status,
          await parameterUpdateResponse.text(),
        )
      }
      /* const parameterUpdate = */ await parameterUpdateResponse.json()
      // console.log("parameterUpdate", JSON.stringify(parameterUpdate, null, 2))

      // Create pull request.
      const pullRequestUrl = `https://api.${parametersRepository.forgeDomainName}/repos/${parametersRepository.group}/${parametersRepository.project}/pulls`
      const pullRequestResponse = await fetch(pullRequestUrl, {
        body: JSON.stringify(
          {
            base: parametersRepository.branch,
            // body:
            draft: false,
            head: newBranchName,
            // issue:
            maintainer_can_modify: true,
            title: `Modification du paramètre ${name}`,
          },
          null,
          2,
        ),
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${parametersRepository.accessToken!}`,
          "Content-Type": "application/json",
          "X-GitHub-Api-Version": "2022-11-28",
        },
        method: "POST",
      })
      if (!pullRequestResponse.ok) {
        console.error(`Error while creating pull request at ${pullRequestUrl}`)
        error(pullRequestResponse.status, await pullRequestResponse.text())
      }
      const pullRequest = (await pullRequestResponse.json()) as {
        // Incomplete typing
        html_url: string // Merge request URL
        number: number //
      }
      // console.log("pullRequest", JSON.stringify(pullRequest, null, 2))

      // Add comment with user infos.
      const commentUrl = `https://api.${parametersRepository.forgeDomainName}/repos/${parametersRepository.group}/${parametersRepository.project}/issues/${pullRequest.number}/comments`
      const commentResponse = await fetch(commentUrl, {
        body: JSON.stringify(
          {
            body: privateNoteBody,
          },
          null,
          2,
        ),
        headers: {
          Accept: "application/vnd.github+json",
          Authorization: `Bearer ${parametersRepository.accessToken!}`,
          "Content-Type": "application/json",
          "X-GitHub-Api-Version": "2022-11-28",
        },
        method: "POST",
      })
      if (!commentResponse.ok) {
        console.error(
          `Error while creating pull request comment at ${pullRequestUrl}`,
        )
        error(commentResponse.status, await commentResponse.text())
      }
      /* const comment = */ await commentResponse.json()
      // console.log("comment", JSON.stringify(comment, null, 2))

      return json({
        iid: pullRequest.number.toString(),
        web_url: pullRequest.html_url,
      })
    }

    case "GitLab": {
      // Create new branch.
      const newBranchName = `edit_param_${name}_${randomBytes(4).toString("hex")}`
      const newBranchUrl = `https://${
        parametersRepository.forgeDomainName
      }/api/v4/projects/${encodeURIComponent(
        `${parametersRepository.group}/${parametersRepository.project}`,
      )}/repository/branches?branch=${newBranchName}&ref=${encodeURIComponent(
        parametersRepository.branch,
      )}`
      const newBranchResponse = await fetch(newBranchUrl, {
        body: "",
        headers: {
          Accept: "application/json",
          "PRIVATE-TOKEN": parametersRepository.accessToken!,
        },
        method: "POST",
      })
      if (!newBranchResponse.ok) {
        error(newBranchResponse.status, await newBranchResponse.text())
      }

      // Update parameter & commit it.
      const parameterUpdateUrl = `https://${
        parametersRepository.forgeDomainName
      }/api/v4/projects/${encodeURIComponent(
        `${parametersRepository.group}/${parametersRepository.project}`,
      )}/repository/files/${encodeURIComponent(embeddedParameter.file_path!)}`
      const parameterUpdateResponse = await fetch(parameterUpdateUrl, {
        body: JSON.stringify(
          {
            author_email: parametersRepository.authorEmail,
            author_name: parametersRepository.authorName,
            branch: newBranchName,
            commit_message: `Modification du paramètre ${name}`,
            content: rawParameterYaml,
          },
          null,
          2,
        ),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "PRIVATE-TOKEN": parametersRepository.accessToken!,
        },
        method: "PUT",
      })
      if (!parameterUpdateResponse.ok) {
        error(
          parameterUpdateResponse.status,
          await parameterUpdateResponse.text(),
        )
      }
      /* const parameterUpdate = */ await parameterUpdateResponse.json()

      // Create merge request.
      const mergeRequestUrl = `https://${
        parametersRepository.forgeDomainName
      }/api/v4/projects/${encodeURIComponent(
        `${parametersRepository.group}/${parametersRepository.project}`,
      )}/merge_requests`
      const mergeRequestResponse = await fetch(mergeRequestUrl, {
        body: JSON.stringify(
          {
            allow_collaboration: true,
            remove_source_branch: true,
            source_branch: newBranchName,
            target_branch: parametersRepository.branch,
            title: `Modification du paramètre ${name}`,
          },
          null,
          2,
        ),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "PRIVATE-TOKEN": parametersRepository.accessToken!,
        },
        method: "POST",
      })
      if (!mergeRequestResponse.ok) {
        error(mergeRequestResponse.status, await mergeRequestResponse.text())
      }
      const mergeRequest = (await mergeRequestResponse.json()) as {
        // Incomplete typing
        iid: string
        web_url: string // Merge request URL
      }

      // Add private note containing user infos to merge request.
      const noteUrl = `https://${
        parametersRepository.forgeDomainName
      }/api/v4/projects/${encodeURIComponent(
        `${parametersRepository.group}/${parametersRepository.project}`,
      )}/merge_requests/${mergeRequest.iid}/notes`
      const noteResponse = await fetch(noteUrl, {
        body: JSON.stringify(
          {
            body: privateNoteBody,
            internal: true,
          },
          null,
          2,
        ),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "PRIVATE-TOKEN": parametersRepository.accessToken!,
        },
        method: "POST",
      })
      if (!noteResponse.ok) {
        error(noteResponse.status, await noteResponse.text())
      }
      /* const note = */ await noteResponse.json()

      return json(mergeRequest)
    }

    default: {
      assertNever(
        "packages/editor/src/routes/[[languageCode=language_code]]/[[parameter]]/+server.ts PUT ",
        parametersRepository.forgeType,
      )
    }
  }
}
