import { error } from "@sveltejs/kit"

import type { PageLoad } from "./$types"

export const load: PageLoad = async function ({ parent }) {
  const { embeddedParameter, fileParameter } = await parent()

  if (fileParameter === undefined) {
    error(
      400,
      `L'édition du paramètre "${
        embeddedParameter.name ?? "racine"
      }" est impossible car il n'a pas de fichier YAML qui lui soit propre.`,
    )
  }

  return {}
}
