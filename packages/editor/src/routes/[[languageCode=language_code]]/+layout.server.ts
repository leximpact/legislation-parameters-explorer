import "$lib/i18n" // Import to initialize. Important :)
import { locale, waitLocale } from "svelte-i18n"

import { defaultLanguageCode, type LanguageCode } from "$lib/i18n"

import type { LayoutServerLoad } from "./$types"

export const load: LayoutServerLoad = async ({ params }) => {
  const languageCode =
    (params.languageCode as LanguageCode | undefined) ?? defaultLanguageCode
  locale.set(languageCode)
  await waitLocale()
  return {
    languageCode,
  }
}
