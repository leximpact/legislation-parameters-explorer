export interface SigninPayload {
  nonce: string
  redirectUrl: string
  token: string
}

export interface SignoutPayload {
  redirectUrl: string
}

export interface GithubUser {
  email: string
  id: number
  issuerName: "github" // Added
  login: string
  name: string
  username: string // Added
  // Other GitHub attributes…
}

export interface GitlabUser {
  aud: string // 64 hex digits
  auth_time: number // 1701961788
  email: string
  email_verified: boolean
  groups_direct: string[] // pathnames of groups the user belongs to
  iss: "https://gitlab.com"
  issuerName: string // Added
  name: string // Fullname "John Doe"
  nickname: string // "jdoe"
  nonce: string // "Zro4vL-rQbtMWnNEKYJ8nr-eTi-9kjc1iI-TOwedkKo"
  picture: string // "https://gitlab.com/uploads/-/system/user/avatar/${sub}/avatar.png"
  preferred_username: string // "jdoe"
  profile: string // "https://gitlab.com/${preferred_username}"
  sub: string // user numeric ID "123456"
  sub_legacy: string // 64 hex digits
  username: string // Added
}

export type User = GithubUser | GitlabUser
