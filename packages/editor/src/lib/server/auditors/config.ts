import {
  auditCleanArray,
  auditEmptyToNull,
  auditFunction,
  auditHttpUrl,
  auditOptions,
  auditRequire,
  auditSetNullish,
  auditTrimString,
  cleanAudit,
  type Audit,
} from "@auditors/core"

import { customizations } from "$lib/customizations"
import { forgeTypes } from "$lib/repositories"
import type { OpenIdConnectConfig } from "$lib/server/config"

export function auditConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "customization",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditOptions(customizations),
  )
  audit.attribute(data, "github", true, errors, remainingKeys, auditGithub)
  for (const key of ["jwtSecret", "parametersDir", "title", "unitsFilePath"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "openIdConnectByName",
    true,
    errors,
    remainingKeys,
    auditCleanArray(auditOpenIdConnect),
    auditSetNullish([]),
    auditFunction((configs) =>
      Object.fromEntries(
        (configs as OpenIdConnectConfig[]).map((config) => [
          config.title.toLocaleLowerCase(),
          config,
        ]),
      ),
    ),
  )
  audit.attribute(
    data,
    "parametersRepository",
    true,
    errors,
    remainingKeys,
    auditRepositoryConfig,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditGithub(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of [
    "authorizationUrl",
    "issuerUrl",
    "tokenUrl",
    "userInfoUrl",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }
  for (const key of ["clientId", "clientSecret"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "endSessionUrl",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditOpenIdConnect(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["clientId", "clientSecret", "title"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "issuerUrl",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRepositoryConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "accessToken",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditEmptyToNull,
  )
  for (const key of [
    "authorEmail",
    "authorName",
    "branch",
    "forgeDomainName",
    "group",
    "parametersDir",
    "project",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "forgeType",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditOptions(forgeTypes),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: unknown): [unknown, unknown] {
  return auditConfig(cleanAudit, data)
}
