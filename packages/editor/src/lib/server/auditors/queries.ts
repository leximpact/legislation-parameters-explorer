import {
  type Audit,
  auditHttpUrl,
  type Auditor,
  auditSetNullish,
  auditTest,
  cleanAudit,
} from "@auditors/core"

import { auditQuerySingleton } from "$lib/auditors/queries"

function auditLoginLogoutQuery(baseUrl: string): Auditor {
  return function auditLoginLogoutQuery(
    audit: Audit,
    query: URLSearchParams,
  ): [unknown, unknown] {
    if (query == null) {
      return [query, null]
    }
    if (!(query instanceof URLSearchParams)) {
      return audit.unexpectedType(query, "URLSearchParams")
    }

    const data: { [key: string]: string[] } = {}
    for (const [key, value] of query.entries()) {
      let values = data[key]
      if (values === undefined) {
        values = data[key] = []
      }
      values.push(value)
    }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "redirect",
      true,
      errors,
      remainingKeys,
      auditQuerySingleton(
        auditHttpUrl,
        auditTest((url) => url.startsWith(baseUrl)),
        auditSetNullish(baseUrl),
      ),
    )

    return audit.reduceRemaining(
      data,
      errors,
      remainingKeys,
      auditSetNullish({}),
    )
  }
}

export function validateLoginLogoutQuery(baseUrl: string) {
  return function validateLoginLogoutQuery(
    query: URLSearchParams,
  ): [unknown, unknown] {
    return auditLoginLogoutQuery(baseUrl)(cleanAudit, query)
  }
}
