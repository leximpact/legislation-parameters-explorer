import type { Handle } from "@sveltejs/kit"
import { Configuration, discovery } from "openid-client"

import config from "$lib/server/config"

export interface OpenIdConnectLocals {
  openIdConnectConfigurationByName?: { [name: string]: Configuration }
}

const { openIdConnectByName } = config

export const openIdConnectHandler: Handle = async ({ event, resolve }) => {
  if (event.locals.openIdConnectConfigurationByName === undefined) {
    const openIdConnectConfigurationByName: { [name: string]: Configuration } =
      {}
    for (const [name, openIdConnect] of Object.entries(
      openIdConnectByName ?? {},
    )) {
      openIdConnectConfigurationByName[name] = await discovery(
        new URL(openIdConnect.issuerUrl),
        openIdConnect.clientId,
        openIdConnect.clientSecret,
      )
      openIdConnectConfigurationByName[name].timeout = 10000 // 10 s
    }
    event.locals.openIdConnectConfigurationByName =
      openIdConnectConfigurationByName
  }

  return await resolve(event)
}
