import "dotenv/config"

import type { Customization } from "$lib/customizations"
import type { RepositoryConfig } from "$lib/repositories"
import { validateConfig } from "$lib/server/auditors/config"

export interface Config {
  customization?: Customization
  jwtSecret: string
  github: {
    authorizationUrl: string
    clientId: string
    clientSecret: string
    endSessionUrl?: string
    issuerUrl: string
    tokenUrl: string
    userInfoUrl: string
  }
  openIdConnectByName: { [name: string]: OpenIdConnectConfig }
  parametersDir: string
  parametersRepository: RepositoryConfig
  title: string
  unitsFilePath: string
}

export interface OpenIdConnectConfig {
  clientId: string
  clientSecret: string
  issuerUrl: string
  title: string
}

function* iterOpenIdConnectConfigs() {
  for (
    let i = 1;
    process.env[`OPENID_CONNECT_ISSUER_URL${i}`] !== undefined;
    i++
  ) {
    yield {
      clientId: process.env[`OPENID_CONNECT_CLIENT_ID${i}`],
      clientSecret: process.env[`OPENID_CONNECT_CLIENT_SECRET${i}`],
      issuerUrl: process.env[`OPENID_CONNECT_ISSUER_URL${i}`],
      title: process.env[`OPENID_CONNECT_TITLE${i}`],
    }
  }
}

const [config, error] = validateConfig({
  customization: process.env["CUSTOMIZATION"],
  jwtSecret: process.env["JWT_SECRET"],
  github: process.env["GITHUB_ISSUER_URL"]
    ? {
        authorizationUrl: process.env["GITHUB_AUTHORIZATION_URL"],
        clientId: process.env["GITHUB_CLIENT_ID"],
        clientSecret: process.env["GITHUB_CLIENT_SECRET"],
        endSessionUrl: process.env["GITHUB_END_SESSION_URL"],
        issuerUrl: process.env["GITHUB_ISSUER_URL"],
        tokenUrl: process.env["GITHUB_TOKEN_URL"],
        userInfoUrl: process.env["GITHUB_USERINFO_URL"],
      }
    : undefined,
  openIdConnectByName: [...iterOpenIdConnectConfigs()],
  parametersDir: process.env["PARAMETERS_DIR"],
  parametersRepository: {
    accessToken: process.env["PARAMETERS_ACCESS_TOKEN"],
    authorEmail: process.env["PARAMETERS_AUTHOR_EMAIL"],
    authorName: process.env["PARAMETERS_AUTHOR_NAME"],
    branch: process.env["PARAMETERS_BRANCH"],
    forgeDomainName: process.env["PARAMETERS_FORGE_DOMAIN_NAME"],
    forgeType: process.env["PARAMETERS_FORGE_TYPE"],
    group: process.env["PARAMETERS_GROUP"],
    parametersDir: process.env["PARAMETERS_PROJECT_DIR"],
    project: process.env["PARAMETERS_PROJECT"],
  },
  title: process.env["TITLE"],
  unitsFilePath: process.env["UNITS_FILE_PATH"],
}) as [Config, unknown]
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      config,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default config
