import type { Handle } from "@sveltejs/kit"
import cookie from "cookie"

export interface CookiesLocals {
  cookies?: { [key: string]: string }
}

export const cookiesHandler: Handle = async ({ event, resolve }) => {
  const cookieHeader = event.request.headers.get("Cookie")
  event.locals.cookies = cookieHeader === null ? {} : cookie.parse(cookieHeader)

  return await resolve(event)
}
