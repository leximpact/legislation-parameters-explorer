import type { Handle } from "@sveltejs/kit"
import { Configuration } from "openid-client"

import config from "$lib/server/config"

export interface GithubLocals {
  githubConfiguration?: Configuration
}

const { github } = config

export const githubHandler: Handle = async ({ event, resolve }) => {
  if (github !== undefined) {
    if (event.locals.githubConfiguration === undefined) {
      event.locals.githubConfiguration = new Configuration(
        {
          authorization_endpoint: github.authorizationUrl,
          end_session_endpoint: github.endSessionUrl,
          issuer: github.issuerUrl,
          token_endpoint: github.tokenUrl,
          userinfo_endpoint: github.userInfoUrl,
        },
        github.clientId,
        github.clientSecret,
      )
      event.locals.githubConfiguration.timeout = 10000 // 10 s
    }
  }

  return await resolve(event)
}
