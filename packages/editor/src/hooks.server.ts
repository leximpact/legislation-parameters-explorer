import type { Handle } from "@sveltejs/kit"
import { sequence } from "@sveltejs/kit/hooks"

import {
  allLanguageCodes,
  defaultLanguageCode,
  type LanguageCode,
} from "$lib/i18n"
import { githubHandler } from "$lib/server/github_handler"
import { openIdConnectHandler } from "$lib/server/openid_connect_handler"
import { csrfHandler } from "$lib/server/csrf_handler"
import { userHandler } from "$lib/server/user_handler"

export const handle: Handle = sequence(
  csrfHandler([
    /^\/auth\/(.*?)\/login_callback$/,
    /^\/auth\/(.*?)\/logout_callback$/,
  ]),
  userHandler,
  githubHandler,
  openIdConnectHandler,
  async ({ event, resolve }) => {
    const pathname = event.url.pathname
    const match = pathname.match(/^\/([a-z]{2})(\/|$)/)
    const languageCode =
      match === null
        ? defaultLanguageCode
        : allLanguageCodes.includes(match[1] as LanguageCode)
          ? (match[1] as LanguageCode)
          : defaultLanguageCode
    return resolve(event, {
      transformPageChunk: ({ html }) => html.replace("%lang%", languageCode),
    })
  },
)
