// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces

import "unplugin-icons/types/svelte"

type Unit = import("@tax-benefit/openfisca-json-model").Unit

declare global {
  namespace App {
    // interface Error {}

    type GithubLocals = import("$lib/server/github_handler").GithubLocals
    type OpenIdConnectLocals =
      import("$lib/server/openid_connect_handler").OpenIdConnectLocals
    type UserLocals = import("$lib/server/user_handler").UserLocals
    interface Locals extends UserLocals, GithubLocals, OpenIdConnectLocals {
      id_token?: string
      user_id?: string
    }

    interface PageData {
      appTitle: string
      hasRespositoryToken: boolean
      openIdConnectTitleByName: { [name: string]: string }
      parametersRepository: import("$lib/config").RepositoryConfig
      rootParameter: Parameter
      unitByName: { [name: string]: Unit }
      units: Unit[]
      user?: import("$lib/users").User
    }

    // interface PageState {}
    // interface Platform {}
  }
}

export {}
