import adapter from "@sveltejs/adapter-static"
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte"

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    // See https://kit.svelte.dev/docs/adapters for more information about adapters.
    adapter: adapter({
      fallback: "error_404.html",
    }),

    embedded: Boolean(process.env.EMBEDDED),

    paths: {
      assets: process.argv.includes("dev")
        ? ""
        : (process.env.ASSETS_URL ?? ""),
      base: process.argv.includes("dev") ? "" : (process.env.BASE_PATH ?? ""),
      // Use absolute paths to ensure that the path of the CSS is the same for
      // /x/ and /x/index.html (needed for GitLab pages).
      // See https://github.com/sveltejs/kit/issues/9404
      relative: false,
    },

    prerender: {
      handleHttpError: "warn",
    },
  },

  // Consult https://kit.svelte.dev/docs/integrations#preprocessors
  // for more information about preprocessors
  preprocess: vitePreprocess(),
}

export default config
