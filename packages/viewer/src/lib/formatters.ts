import {
  getUnitAtDate,
  type ConstantUnit,
  type Unit,
} from "@tax-benefit/openfisca-json-model"

import type { LanguageCode } from "$lib/i18n"
import { getUnitShortLabel } from "$lib/units"

export function formatCellNumber(
  value: number,
  unitByName: { [name: string]: Unit },
  unit: string | undefined,
  date: string,
  languageCode: LanguageCode,
): string {
  if (unit === undefined) {
    return formatNumber(value, languageCode)
  }
  // Special handling for "year", to remove the thousands separators.
  // For percentage, use a trick to round value * 100
  return `${unit === "year" ? value.toString() : formatNumber((getUnitAtDate(unitByName, unit, date) as ConstantUnit)?.ratio ? parseFloat((value * 100).toFixed(8)) : value, languageCode)} ${getUnitShortLabel(unitByName, unit, date)}`
}

export function formatNumber(
  value: number,
  languageCode: LanguageCode,
  options?: Intl.NumberFormatOptions,
): string {
  if (options && options.currency == "AFRF") {
    return formatNumber(value, languageCode, {
      ...options,
      currency: "FRF",
    }).replace("F", "AF")
  }
  return new Intl.NumberFormat(languageCode, {
    maximumFractionDigits: 10,
    ...options,
  }).format(value)
}
