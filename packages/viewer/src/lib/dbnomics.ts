export interface DbnomicsConfig {
  datasetCode: string
  providerCode: string
  url: string
}
