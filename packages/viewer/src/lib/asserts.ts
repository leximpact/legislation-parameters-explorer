export function assertNever(name: string, value: never): never {
  throw `Unexpected value for "${name}" : ${value}`
}
