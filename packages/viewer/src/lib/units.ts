import {
  getUnitLabel as getUnitLabelOriginal,
  getUnitShortLabel as getUnitShortLabelOriginal,
  type PeriodUnit,
  type Unit,
} from "@tax-benefit/openfisca-json-model"

const frenchPluralRules = new Intl.PluralRules(["fr-FR"])

export function getUnitLabel(
  unitByName: { [name: string]: Unit },
  name: string | undefined | null,
  date: string | undefined | null,
  { periodUnit, value = 123 }: { periodUnit?: PeriodUnit; value?: number } = {},
): string {
  const unitLabel = getUnitLabelOriginal(
    unitByName,
    name,
    date,
    frenchPluralRules.select(value ?? 0),
  )
  if (
    name === "/1" ||
    unitLabel === "" ||
    periodUnit === undefined ||
    !["day", "month", "year"].includes(periodUnit)
  ) {
    return unitLabel
  }
  return `${unitLabel}/an`
}

export function getUnitShortLabel(
  unitByName: { [name: string]: Unit },
  name: string | undefined | null,
  date: string | undefined | null,
  { periodUnit, value = 123 }: { periodUnit?: PeriodUnit; value?: number } = {},
): string {
  const unitShortLabel = getUnitShortLabelOriginal(
    unitByName,
    name,
    date,
    frenchPluralRules.select(value ?? 0),
  )
  if (
    name === "/1" ||
    unitShortLabel === "" ||
    periodUnit === undefined ||
    !["day", "month", "year"].includes(periodUnit)
  ) {
    return unitShortLabel
  }
  return `${unitShortLabel}/an`
}
