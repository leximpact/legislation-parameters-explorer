import { assertNever } from "$lib/asserts"
import type { LanguageCode } from "$lib/i18n"

export interface TableOfContentsIndex {
  children: Array<
    TableOfContentsIndex | string // ID of a TableOfContentsItem
  >
  title: Title
}

export interface TableOfContentsItem {
  depth?: number
  income_tax_year?: boolean
  name_en?: string // Used only for root table of contents items
  subparams?: TableOfContentsItemSubparam[]
  // Name of an OpenFisca Node parameter to display as a tree
  // Can't be present when `subparams` is present.
  subsection?: string
  title: Title
}

export type TableOfContentsItemSubparam =
  | TableOfContentsItem
  | TableOfContentsItemSubparamSubsection
  | TableOfContentsItemSubparamTable

export interface TableOfContentsItemSubparamSubsection {
  depth?: number
  exclude?: string[]
  flat?: boolean
  income_tax_year?: boolean
  // Name of an OpenFisca Node parameter to display as a tree
  subsection: string
}

export interface TableOfContentsItemSubparamTable {
  // Relative names of sub-parameters to exclude from tables
  exclude?: string[]
  income_tax_year?: boolean
  // Name of an OpenFisca parameter to display as a table
  table: string
}

export interface Title {
  en?: string
  fr: string
}

export function getTableOfContentsItemId(
  tableOfContentsItem: TableOfContentsItem,
  languageCode: LanguageCode,
  defaultId: string,
) {
  switch (languageCode) {
    case "en":
      return tableOfContentsItem.name_en ?? defaultId
    case "fr":
      return defaultId
    default:
      assertNever("languageCode", languageCode)
  }
}

export function textFromYearRange({
  firstYear,
  lastYear,
}: {
  firstYear?: number
  lastYear?: number
}): string {
  return firstYear === undefined
    ? ""
    : lastYear === undefined
      ? ` (depuis ${firstYear})`
      : lastYear === firstYear
        ? ` (${firstYear})`
        : ` (${firstYear}-${lastYear})`
}
