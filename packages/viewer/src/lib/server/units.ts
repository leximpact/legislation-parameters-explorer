import { auditChain, auditRequire, strictAudit } from "@auditors/core"
import {
  auditUnits,
  unitsFromYaml,
  type Unit,
} from "@tax-benefit/openfisca-json-model"
import fs from "fs-extra"

import config from "$lib/server/config"

const { unitsFilePath } = config
export const units = loadUnits(unitsFilePath)
export const unitByName = Object.fromEntries(
  units.map((unit) => [unit.name, unit]),
)

function loadUnits(unitsFilePath: string): Unit[] {
  const [units, error] = auditChain(auditUnits, auditRequire)(
    strictAudit,
    unitsFromYaml(fs.readFileSync(unitsFilePath, "utf-8")),
  ) as [Unit[], unknown]
  if (error !== null) {
    console.error(
      `An error occurred when validating units at ${unitsFilePath}:`,
    )
    console.error(JSON.stringify(units, null, 2))
    console.error(JSON.stringify(error, null, 2))
    throw new Error("Units validation failed.")
  }
  return units
}
