import type { NodeParameter } from "@tax-benefit/openfisca-json-model"

import { tableOfContentsItemIdByParameterNameByLanguageCode } from "$lib/data/table_of_contents.json"
import type { LanguageCode } from "$lib/i18n/index"

import { tableOfContents } from "$lib/server/table_of_contents_without_data"

export { tableOfContents } from "$lib/server/table_of_contents_without_data"

// Caution: This function modifies ancestors array in place.
export function hideParameterAncestorsMissingFromTableOfContents({
  ancestors,
  languageCode,
}: {
  ancestors: Array<
    NodeParameter & {
      children: undefined
    }
  >
  languageCode: LanguageCode
}): {
  hiddenAncestors: Array<
    NodeParameter & {
      children: undefined
    }
  >
  visibleAncestors: Array<
    NodeParameter & {
      children: undefined
    }
  >
} {
  const hiddenAncestors: Array<
    NodeParameter & {
      children: undefined
    }
  > = [ancestors[0]]
  const visibleAncestors = ancestors.slice(1) // Remove root Parameter.
  if (tableOfContents !== undefined) {
    while (visibleAncestors.length > 0) {
      const ancestor = visibleAncestors[0]
      if (
        (
          tableOfContentsItemIdByParameterNameByLanguageCode[languageCode] as {
            [parameterName: string]: string
          }
        )[ancestor.name as string] !== undefined
      ) {
        break
      }
      hiddenAncestors.push(ancestor)
      visibleAncestors.shift()
    }
  }
  return { hiddenAncestors, visibleAncestors }
}
