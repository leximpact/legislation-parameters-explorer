import {
  auditEmptyToNull,
  auditHttpUrl,
  auditOptions,
  auditRequire,
  auditSetNullish,
  auditStringToBoolean,
  auditTrimString,
  cleanAudit,
  type Audit,
} from "@auditors/core"

import { customizations } from "$lib/customizations"
import { forgeTypes } from "$lib/repositories"

export function auditConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "customization",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditOptions(customizations),
  )
  audit.attribute(
    data,
    "dbnomics",
    true,
    errors,
    remainingKeys,
    auditDbnomicsConfig,
  )
  audit.attribute(data, "editorUrl", true, errors, remainingKeys, auditHttpUrl)
  for (const key of ["exportCsv", "exportJson", "exportXlsx"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditStringToBoolean,
      auditSetNullish(false),
    )
  }
  for (const key of ["parametersDir", "title", "unitsFilePath"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "parametersRepository",
    true,
    errors,
    remainingKeys,
    auditRepositoryConfig,
    auditRequire,
  )
  audit.attribute(
    data,
    "tableOfContentsDir",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditEmptyToNull,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditDbnomicsConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["providerCode", "datasetCode"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRepositoryConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of [
    "authorEmail",
    "authorName",
    "branch",
    "forgeDomainName",
    "group",
    "parametersDir",
    "project",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "forgeType",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditOptions(forgeTypes),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: unknown): [unknown, unknown] {
  return auditConfig(cleanAudit, data)
}
