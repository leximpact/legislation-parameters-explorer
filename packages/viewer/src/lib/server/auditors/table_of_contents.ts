import {
  auditArray,
  auditBoolean,
  auditEmptyToNull,
  auditFunction,
  auditInteger,
  auditMatch,
  auditObject,
  auditOptions,
  auditPartial,
  auditRequire,
  auditSwitch,
  auditTest,
  auditTrimString,
  auditUnique,
  type Audit,
  type Auditor,
} from "@auditors/core"
import {
  ParameterClass,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"

import { getEmbeddedParameter } from "$lib/parameters"

import { auditParameterName } from "./parameters"

export const auditTableOfContentsIndex = (
  itemsId: string[],
  rootParameter: Parameter,
): Auditor =>
  auditObject({
    children: [
      auditArray(
        auditSwitch(
          [
            auditTrimString,
            auditEmptyToNull,
            auditRequire,
            auditOptions(itemsId),
          ],
          (audit, data) =>
            auditTableOfContentsIndex(itemsId, rootParameter)(audit, data),
        ),
        auditRequire,
      ),
      auditRequire,
    ],
    title: [
      auditObject({
        en: [auditTrimString, auditEmptyToNull],
        fr: [auditTrimString, auditEmptyToNull, auditRequire],
      }),
      auditRequire,
    ],
  })

export const auditTableOfContentsItem = (rootParameter: Parameter): Auditor =>
  auditObject({
    depth: auditInteger,
    income_tax_year: auditBoolean,
    name_en: [auditTrimString, auditEmptyToNull],
    subparams: auditArray(
      auditPartial({
        subsection: [auditTrimString, auditEmptyToNull],
        table: [auditTrimString, auditEmptyToNull],
      }),
      auditMatch(
        (subparam) => {
          const { subsection, table } = subparam as {
            subsection?: string
            table?: string
          }
          return subsection === undefined
            ? table === undefined
              ? "table_of_content_item"
              : "parameter_as_table"
            : "node"
        },
        {
          node: (audit: Audit, data: unknown): [unknown, unknown] => {
            const [validData, error] = auditObject({
              depth: auditInteger,
              exclude: auditSwitch(
                [
                  auditTrimString,
                  auditTest(
                    (value) => !value.includes("."),
                    'Only a direct child (without "." in its ID) can be excluded',
                  ),
                  auditEmptyToNull,
                  auditFunction((value) => [value]),
                ],
                [
                  auditArray(
                    auditTrimString,
                    auditTest(
                      (value) => !value.includes("."),
                      'Only a direct child (without "." in its ID) can be excluded',
                    ),
                    auditEmptyToNull,
                    auditRequire,
                  ),
                  auditUnique,
                  auditEmptyToNull,
                ],
              ),
              flat: auditBoolean,
              income_tax_year: auditBoolean,
              subsection: [
                auditTrimString,
                auditEmptyToNull,
                auditParameterName(rootParameter),
                auditRequire,
              ],
            })(audit, data) as [
              {
                depth?: number
                exclude?: string[]
                flat?: boolean
                subsection: string
              },
              unknown,
            ]
            if (error !== null) {
              return [data, error]
            }
            const parameter = getEmbeddedParameter(
              rootParameter,
              validData.subsection,
            ).embeddedParameter
            if (
              parameter !== undefined &&
              parameter.class !== ParameterClass.Node &&
              (validData.depth !== undefined ||
                validData.exclude !== undefined ||
                validData.flat !== undefined)
            ) {
              return [
                data,
                {
                  depth:
                    validData.depth === undefined
                      ? undefined
                      : `Unexpected value: Parameter ${validData.subsection} is a ${parameter?.class} and not a Node.`,
                  exclude:
                    validData.exclude === undefined
                      ? undefined
                      : `Unexpected value: Parameter ${validData.subsection} is a ${parameter?.class} and not a Node.`,
                  flat:
                    validData.flat === undefined
                      ? undefined
                      : `Unexpected value: Parameter ${validData.subsection} is a ${parameter?.class} and not a Node.`,
                },
              ]
            }
            return [validData, null]
          },
          parameter_as_table: auditObject({
            exclude: auditSwitch(
              [
                auditTrimString,
                auditEmptyToNull,
                auditFunction((value) => [value]),
              ],
              [
                auditArray(auditTrimString, auditEmptyToNull, auditRequire),
                auditUnique,
                auditEmptyToNull,
              ],
            ),
            income_tax_year: auditBoolean,
            table: [
              auditTrimString,
              auditEmptyToNull,
              auditParameterName(rootParameter),
              auditRequire,
            ],
          }),
          table_of_content_item: (audit, data) =>
            auditTableOfContentsItem(rootParameter)(audit, data),
        },
      ),
      auditRequire,
    ),
    subsection: [
      auditTrimString,
      auditEmptyToNull,
      auditParameterName(rootParameter),
    ],
    title: [
      auditObject({
        en: [auditTrimString, auditEmptyToNull],
        fr: [auditTrimString, auditEmptyToNull, auditRequire],
      }),
      auditRequire,
    ],
  })
