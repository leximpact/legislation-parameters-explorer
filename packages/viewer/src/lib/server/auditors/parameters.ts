import { auditTest, type Auditor } from "@auditors/core"
import type { Parameter } from "@tax-benefit/openfisca-json-model"

import { getEmbeddedParameter } from "$lib/parameters"

export const auditParameterName = (rootParameter: Parameter): Auditor =>
  auditTest(
    (name) =>
      getEmbeddedParameter(rootParameter, name).embeddedParameter !== undefined,
    "Parameter not found",
  )
