import { auditChain, auditRequire, strictAudit } from "@auditors/core"
import {
  auditRawParameterToEditable,
  ParameterClass,
  rawParameterFromYaml,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"
import fs from "fs-extra"
import path from "path"

import config from "$lib/server/config"
import { units } from "$lib/server/units"

const { parametersDir, parametersRepository } = config

export const rootParameter = loadParameters(parametersDir)
improveParameter(rootParameter)

export function improveParameter(
  parameter: Parameter,
  ids: string[] = [],
): void {
  const id = ids[ids.length - 1]
  if (id !== undefined) {
    parameter.id = id
  }
  if (parameter.name === undefined) {
    parameter.name = ids.join(".")
  }
  // TODO: Remove parameter.title & use getParameterLongTitle or getParameterShortTitle.
  const title =
    parameter.short_label ??
    parameter.description ??
    id?.replace(/_/g, " ").replace(/^\w/, (c) => c.toUpperCase())
  if (title !== undefined) {
    parameter.title = title
  }

  switch (parameter.class) {
    case ParameterClass.Node:
      if (parameter.children !== undefined) {
        for (const [childId, child] of Object.entries(parameter.children)) {
          improveParameter(child, [...ids, childId])
        }
      }
      break
    default:
  }
}

function loadParameters(parametersDir: string): Parameter {
  const rootRawParameter = loadRawParameters(parametersDir)
  const [editableParameter, error] = auditChain(
    auditRawParameterToEditable(units),
    auditRequire,
  )(strictAudit, rootRawParameter) as [Parameter, unknown]
  if (error !== null) {
    console.error(
      `An error occurred when converting raw parameters at ${parametersDir} to editable:`,
    )
    console.error(JSON.stringify(editableParameter, null, 2))
    console.error(JSON.stringify(error, null, 2))
    throw new Error(
      `Conversion of raw parameters to editable failed:\n${JSON.stringify(editableParameter, null, 2)}\n\nError: ${JSON.stringify(error, null, 2)}`,
    )
  }
  return editableParameter
}

function loadRawParameters(
  parametersDir: string,
  relativeSplitPath: string[] = [],
): unknown {
  const nodePath = path.join(parametersDir, ...relativeSplitPath)
  let rawParameter: { [key: string]: unknown }
  if (fs.statSync(nodePath).isDirectory()) {
    const indexFilePath = path.join(nodePath, "index.yaml")
    if (fs.pathExistsSync(indexFilePath)) {
      rawParameter = rawParameterFromYaml(
        fs.readFileSync(indexFilePath, "utf-8"),
      ) as { [key: string]: unknown }
      rawParameter.file_path = path.join(
        parametersRepository.parametersDir,
        ...relativeSplitPath,
        "index.yaml",
      )
    } else {
      rawParameter = {}
    }
    for (const childFilename of fs.readdirSync(nodePath)) {
      if (childFilename[0] === "." || childFilename === "index.yaml") {
        continue
      }
      const childRelativeSplitPath = [...relativeSplitPath, childFilename]
      const child = loadRawParameters(parametersDir, childRelativeSplitPath)
      const childId = childFilename.replace(/\.yaml$/, "")
      ;(rawParameter as { [key: string]: unknown })[childId] = child
    }
  } else {
    if (!fs.pathExistsSync(nodePath)) {
      console.error(`Missing parameter file at ${nodePath}`)
      return undefined
    }
    rawParameter = rawParameterFromYaml(fs.readFileSync(nodePath, "utf-8")) as {
      [key: string]: unknown
    }
    if (rawParameter === undefined) {
      console.error(`Invalid parameter file at ${nodePath}`)
      return undefined
    }
    rawParameter.file_path = path.join(
      parametersRepository.parametersDir,
      ...relativeSplitPath,
    )
  }
  const name = relativeSplitPath
    .join(".")
    .replace(/\.yaml$/, "")
    .replace(/\.index$/, "")
  ;(rawParameter as { [key: string]: unknown }).name = name
  return rawParameter
}
