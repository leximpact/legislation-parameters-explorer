import "dotenv/config"

import type { Customization } from "$lib/customizations"
import type { DbnomicsConfig } from "$lib/dbnomics"
import type { RepositoryConfig } from "$lib/repositories"
import { validateConfig } from "$lib/server/auditors/config"

export interface Config {
  customization?: Customization
  dbnomics?: DbnomicsConfig
  editorUrl?: string
  exportCsv: boolean
  exportJson: boolean
  exportXlsx: boolean
  parametersDir: string
  parametersRepository: RepositoryConfig
  tableOfContentsDir?: string
  title: string
  unitsFilePath: string
}

const [config, error] = validateConfig({
  customization: process.env["CUSTOMIZATION"],
  dbnomics: process.env["DBNOMICS_URL"]?.trim()
    ? {
        datasetCode: process.env["DBNOMICS_DATASET_CODE"],
        providerCode: process.env["DBNOMICS_PROVIDER_CODE"],
        url: process.env["DBNOMICS_URL"],
      }
    : undefined,
  editorUrl: process.env["EDITOR_URL"],
  exportCsv: process.env["EXPORT_CSV"],
  exportJson: process.env["EXPORT_JSON"],
  exportXlsx: process.env["EXPORT_XLSX"],
  parametersDir: process.env["PARAMETERS_DIR"],
  parametersRepository: {
    authorEmail: process.env["PARAMETERS_AUTHOR_EMAIL"],
    authorName: process.env["PARAMETERS_AUTHOR_NAME"],
    branch: process.env["PARAMETERS_BRANCH"],
    forgeDomainName: process.env["PARAMETERS_FORGE_DOMAIN_NAME"],
    forgeType: process.env["PARAMETERS_FORGE_TYPE"],
    group: process.env["PARAMETERS_GROUP"],
    parametersDir: process.env["PARAMETERS_PROJECT_DIR"],
    project: process.env["PARAMETERS_PROJECT"],
  },
  tableOfContentsDir: process.env["TABLE_OF_CONTENTS_DIR"],
  title: process.env["TITLE"],
  unitsFilePath: process.env["UNITS_FILE_PATH"],
}) as [Config, unknown]
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      config,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default config
