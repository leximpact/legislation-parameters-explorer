// Since this module is used to generate JSON in `src/data`, it must not import it.

import { auditChain, auditRequire, strictAudit } from "@auditors/core"
import {
  ParameterClass,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"
import fs from "fs-extra"
import path from "path"
import yaml from "js-yaml"

import { allLanguageCodes } from "$lib/i18n/index"
import {
  getEmbeddedParameter,
  iterNodeParameterOrderedChildren,
  type ParameterViewOptions,
} from "$lib/parameters"
import {
  auditTableOfContentsIndex,
  auditTableOfContentsItem,
} from "$lib/server/auditors/table_of_contents"
import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"
import type {
  TableOfContentsIndex,
  TableOfContentsItem,
  TableOfContentsItemSubparamSubsection,
  TableOfContentsItemSubparamTable,
} from "$lib/table_of_contents"

const { tableOfContentsDir } = config
export const tableOfContents = loadTableOfContents(
  rootParameter,
  tableOfContentsDir,
)

function loadTableOfContents(
  rootParameter: Parameter,
  tableOfContentsDir?: string,
):
  | {
      tableOfContentsIndex: TableOfContentsIndex
      tableOfContentsItemByIdByLanguageCode: {
        [languageCode: string]: { [itemId: string]: TableOfContentsItem }
      }
    }
  | undefined {
  if (tableOfContentsDir === undefined) {
    return undefined
  }
  if (!fs.pathExistsSync(tableOfContentsDir)) {
    console.error(
      `Tables directory not found: ${tableOfContentsDir}. Ignoring it.`,
    )
    return undefined
  }
  const tableOfContentsItemByIdByLanguageCode: {
    [languageCode: string]: { [itemId: string]: TableOfContentsItem }
  } = Object.fromEntries(
    allLanguageCodes.map((languageCode) => [languageCode, {}]),
  )
  for (const filename of fs.readdirSync(tableOfContentsDir)) {
    const filePath = path.join(tableOfContentsDir, filename)
    if (!filename.endsWith(".yaml") || filename === "index.yaml") {
      if (!["index.yaml", "README.md"].includes(filename)) {
        console.warn(`Ignoring file ${filePath}`)
      }
      continue
    }

    const tableOfContentsItemJsonObject = yaml.load(
      fs.readFileSync(filePath, "utf-8"),
      {
        schema: yaml.JSON_SCHEMA, // Keep dates as strings.
      },
    )
    const [tableOfContentsItem, error] = auditChain(
      auditTableOfContentsItem(rootParameter),
      auditRequire,
    )(strictAudit, tableOfContentsItemJsonObject) as [
      TableOfContentsItem,
      unknown,
    ]
    if (error !== null) {
      console.error(
        `An error occurred when validating table of contents item at ${filePath}:`,
      )
      console.error(JSON.stringify(tableOfContentsItem, null, 2))
      console.error(JSON.stringify(error, null, 2))
      throw new Error(
        `Validation of table of contents item failed:\n${JSON.stringify(tableOfContentsItem, null, 2)}\n\nError: ${JSON.stringify(error, null, 2)}`,
      )
    }
    const itemId = filename.replace(/\.yaml$/, "")
    tableOfContentsItemByIdByLanguageCode.fr[itemId] = tableOfContentsItem
    tableOfContentsItemByIdByLanguageCode.en[
      tableOfContentsItem.name_en ?? itemId
    ] = tableOfContentsItem
  }

  const filePath = path.join(tableOfContentsDir, "index.yaml")
  const tableOfContentsIndexJsonObject = yaml.load(
    fs.readFileSync(filePath, "utf-8"),
    {
      schema: yaml.JSON_SCHEMA, // Keep dates as strings.
    },
  )
  const [tableOfContentsIndex, error] = auditChain(
    auditTableOfContentsIndex(
      Object.keys(tableOfContentsItemByIdByLanguageCode.fr),
      rootParameter,
    ),
    auditRequire,
  )(strictAudit, tableOfContentsIndexJsonObject) as [
    TableOfContentsIndex,
    unknown,
  ]
  if (error !== null) {
    console.error(
      `An error occurred when validating table of contents index at ${filePath}:`,
    )
    console.error(JSON.stringify(tableOfContentsIndex, null, 2))
    console.error(JSON.stringify(error, null, 2))
    throw new Error(
      `Validation of table of contents index failed:\n${JSON.stringify(tableOfContentsIndex, null, 2)}\n\nError: ${JSON.stringify(error, null, 2)}`,
    )
  }

  return { tableOfContentsIndex, tableOfContentsItemByIdByLanguageCode }
}

function* walkParameterParametersName(
  embeddedParameter: Parameter,
  { depth, exclude, /* flat, */ maxDepth }: ParameterViewOptions,
): Generator<string, void, unknown> {
  if (embeddedParameter.class === ParameterClass.Node && depth <= maxDepth) {
    for (const child of iterNodeParameterOrderedChildren(embeddedParameter)) {
      if (!(exclude ?? []).includes(child.id as string)) {
        yield* walkParameterParametersName(child, {
          depth: depth + 1,
          maxDepth,
        })
      }
    }
  } else if (embeddedParameter.name !== undefined) {
    yield embeddedParameter.name
  }
}

export function* walkTableOfContentsItemParametersName(
  tableOfContentsItem: TableOfContentsItem,
): Generator<string, void, unknown> {
  if (tableOfContentsItem.subparams !== undefined) {
    for (const tableOfContentsItemSubparam of tableOfContentsItem.subparams) {
      if (
        (tableOfContentsItemSubparam as TableOfContentsItem).title !== undefined
      ) {
        yield* walkTableOfContentsItemParametersName(
          tableOfContentsItemSubparam as TableOfContentsItem,
        )
      } else if (
        (tableOfContentsItemSubparam as TableOfContentsItemSubparamSubsection)
          .subsection !== undefined
      ) {
        const { depth, exclude, flat, subsection } =
          tableOfContentsItemSubparam as TableOfContentsItemSubparamSubsection
        const { embeddedParameter } = getEmbeddedParameter(
          rootParameter,
          subsection,
        )
        if (embeddedParameter !== undefined) {
          yield* walkParameterParametersName(embeddedParameter, {
            depth: 0,
            exclude,
            flat,
            maxDepth: depth ?? 0,
          })
        }
      } else {
        yield (tableOfContentsItemSubparam as TableOfContentsItemSubparamTable)
          .table
      }
    }
  } else if (tableOfContentsItem.subsection !== undefined) {
    const { embeddedParameter } = getEmbeddedParameter(
      rootParameter,
      tableOfContentsItem.subsection,
    )
    if (
      embeddedParameter !== undefined &&
      embeddedParameter.class === ParameterClass.Node
    ) {
      for (const child of iterNodeParameterOrderedChildren(embeddedParameter)) {
        yield* walkParameterParametersName(child, {
          depth: 1,
          maxDepth: tableOfContentsItem.depth ?? 0,
        })
      }
    }
  }
}
