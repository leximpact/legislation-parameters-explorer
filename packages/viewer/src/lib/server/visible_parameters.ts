import {
  ParameterClass,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"

import { tableOfContentsItemIdByParameterNameByLanguageCode } from "$lib/data/table_of_contents.json"
import { type LanguageCode } from "$lib/i18n/index"
import { tableOfContents } from "$lib/server/table_of_contents"

export function* walkVisibleParameters(
  parameter: Parameter,
  languageCode: LanguageCode,
  parentIsVisible = false,
): Generator<Parameter, void, unknown> {
  const isVisible = Boolean(
    parameter.name &&
      (parentIsVisible ||
        tableOfContents === undefined ||
        (
          tableOfContentsItemIdByParameterNameByLanguageCode[languageCode] as {
            [parameterName: string]: string
          }
        )[parameter.name] !== undefined),
  )
  if (isVisible) {
    yield parameter
  }
  switch (parameter.class) {
    case ParameterClass.Node:
      if (parameter.children !== undefined) {
        for (const child of Object.values(parameter.children)) {
          yield* walkVisibleParameters(child, languageCode, isVisible)
        }
      }
      break
    default:
  }
}
