import { init, register } from "svelte-i18n"

export type LanguageCode = (typeof allLanguageCodes)[number]

export const allLanguageCodes = ["en", "fr"] as const
export const defaultLanguageCode = "fr" as LanguageCode

register("en", () => import("./locales/en.json"))
register("fr", () => import("./locales/fr.json"))

init({
  fallbackLocale: defaultLanguageCode,
  initialLocale: defaultLanguageCode,
})
