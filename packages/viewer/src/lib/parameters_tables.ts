import {
  ParameterClass,
  ScaleType,
  ValueType,
  type AmountBracket,
  type MaybeNumberValue,
  type NodeParameter,
  type OfficialJournalDatesByInstant,
  type Parameter,
  type RateBracket,
  type Reference,
  type ReferencesByInstant,
  type Unit,
} from "@tax-benefit/openfisca-json-model"
import { format, unwrapFunctionStore } from "svelte-i18n"

import { assertNever } from "$lib/asserts"
import { formatCellNumber } from "$lib/formatters"
import { defaultLanguageCode, type LanguageCode } from "$lib/i18n"
import {
  getParameterLongTitle,
  getParameterShortTitle,
  iterNodeParameterOrderedChildren,
  // labelFromScaleType,
  // labelFromValueType,
} from "$lib/parameters"
import type {
  TableOfContentsItem,
  TableOfContentsItemSubparamSubsection,
  TableOfContentsItemSubparamTable,
} from "$lib/table_of_contents"

type DocumentationContext = {
  fragmentByIndex: Map<number, string>
  isSubparam?: boolean
  nextIndex: number
}

type Metadata = {
  notes: ReferencesByInstant
  official_journal_date: { [date: string]: string[] }
  reference: ReferencesByInstant
}

export type ParametersTableColumn = {
  accessor?: (date: string) => unknown | undefined
  colspan?: number
  columns?: ParametersTableColumn[]
  header: {
    longTitle?: string
    shortTitle: string
  }
  id?: string
  sourceUrl?: string
  unit?: string
  width?: number
}

const $_ = unwrapFunctionStore(format)

function buildMetaDataColumns(
  metadata: Partial<Metadata>,
  languageCode: LanguageCode,
): ParametersTableColumn[] {
  const columnInfosByMetadataName = {
    reference: {
      shortTitle: $_("legislativeReferences", { locale: languageCode }),
      width: 1.8,
    },
    official_journal_date: {
      shortTitle: $_("officialJournalPublication", { locale: languageCode }),
      width: 1,
    },
    notes: { shortTitle: $_("notes", { locale: languageCode }), width: 3.5 },
  }
  return (
    Object.entries(columnInfosByMetadataName)
      // .filter(([name]) => metadata[name as keyof Metadata] !== undefined)
      .map(([name, { shortTitle, width }]) =>
        newColumn({
          accessor: (date: string) => metadata[name as keyof Metadata]?.[date],
          header: { shortTitle },
          id: name,
          width,
        }),
      )
  )
}

function buildParameterColumn({
  dates,
  documentationContext,
  editorUrl,
  exclude,
  hiddenAncestors,
  languageCode,
  parameter,
  unitByName,
  visibleAncestors,
}: {
  dates: string[]
  documentationContext: DocumentationContext
  editorUrl?: string
  exclude: string[]
  hiddenAncestors: Array<NodeParameter & { children: undefined }>
  incomeTaxYear?: boolean
  languageCode: LanguageCode
  parameter: Parameter
  unitByName: { [name: string]: Unit }
  visibleAncestors: Array<NodeParameter & { children: undefined }>
}): ParametersTableColumn {
  let longTitle =
    getParameterLongTitle(parameter, languageCode) ?? parameter.id!
  let shortTitle =
    getParameterShortTitle(parameter, languageCode) ?? parameter.id!
  if (documentationContext.isSubparam) {
    if (parameter.documentation !== undefined) {
      let foundFragmentIndex: number | undefined = undefined
      for (const [
        index,
        fragment,
      ] of documentationContext.fragmentByIndex.entries()) {
        if (parameter.documentation === fragment) {
          foundFragmentIndex = index
          break
        }
      }
      if (foundFragmentIndex === undefined) {
        documentationContext.fragmentByIndex.set(
          documentationContext.nextIndex,
          parameter.documentation,
        )
        longTitle = `${longTitle} (${documentationContext.nextIndex})`
        shortTitle = `${shortTitle} (${documentationContext.nextIndex})`
        documentationContext.nextIndex++
      } else if (foundFragmentIndex > 0) {
        longTitle = `${longTitle} (${foundFragmentIndex})`
        shortTitle = `${shortTitle} (${foundFragmentIndex})`
      }
    }
  } else {
    const documentationAncestors = [
      ...hiddenAncestors,
      ...visibleAncestors,
      parameter,
    ]
    const firstAncestorIndex = documentationAncestors.findLastIndex(
      (ancestor) => ancestor.documentation_start,
    )
    const ancestorsDocumentation: string[] = []
    for (const { documentation } of documentationAncestors.slice(
      firstAncestorIndex,
    )) {
      if (
        documentation !== undefined &&
        !ancestorsDocumentation.includes(documentation)
      ) {
        ancestorsDocumentation.push(documentation)
      }
    }
    for (const [index, documentation] of ancestorsDocumentation.entries()) {
      documentationContext.fragmentByIndex.set(
        index - ancestorsDocumentation.length,
        documentation,
      )
    }
  }
  const header =
    shortTitle === longTitle ? { shortTitle } : { longTitle, shortTitle }

  switch (parameter.class) {
    case ParameterClass.Node: {
      documentationContext.isSubparam = true
      return newColumn({
        header,
        columns: [...iterNodeParameterOrderedChildren(parameter)]
          .filter((child) => !exclude.includes(child.id as string))
          .map((child) =>
            buildParameterColumn({
              dates,
              documentationContext,
              editorUrl,
              exclude: exclude.reduce((childExclude, partialName) => {
                const prefix = `${child.id}.`
                if (partialName.startsWith(prefix)) {
                  childExclude.push(partialName.slice(prefix.length))
                }
                return childExclude
              }, [] as string[]),

              hiddenAncestors,
              languageCode,
              parameter: child,
              unitByName,
              visibleAncestors,
            }),
          ),
      })
    }

    case ParameterClass.Scale: {
      let fields: Array<keyof AmountBracket> | Array<keyof RateBracket>
      switch (parameter.type) {
        case ScaleType.LinearAverageRate:
        case ScaleType.MarginalRate: {
          fields = ["base", "threshold", "rate"] as Array<keyof RateBracket>

          let hasBase = false
          for (const bracket of parameter.brackets) {
            if (bracket.base !== undefined) {
              hasBase = true
              break
            }
          }
          if (!hasBase) {
            fields.splice(0, 1) // Remove "base" field.
          }

          break
        }

        case ScaleType.MarginalAmount:
        case ScaleType.SingleAmount: {
          fields = ["threshold", "amount"] as Array<keyof AmountBracket>
          break
        }

        default: {
          assertNever("ScaleParameter.type", parameter)
        }
      }

      let columns: ParametersTableColumn[]
      if (
        fields.length === 2 &&
        parameter.threshold_unit?.startsWith("currency")
      ) {
        // Create base, threshold and rate/amount columns for each bracket.

        const firstBracket = parameter.brackets[0] as RateBracket
        const indexOffset = (fields as Array<keyof RateBracket>).every(
          (field) => {
            const valueByDate = firstBracket[field]
            if (valueByDate === undefined) {
              return true
            }
            const firstValue = Object.entries(valueByDate).toSorted(
              ([date1], [date2]) => date1.localeCompare(date2),
            )[0][1]
            return (
              firstValue === "expected" ||
              firstValue.value === null ||
              firstValue.value === 0
            )
          },
        )
          ? 0
          : 1

        columns = parameter.brackets.map((bracket, index) =>
          newColumn({
            header: {
              shortTitle: $_("trancheN", {
                locale: languageCode,
                values: { index: indexOffset + index },
              }),
            },
            columns: fields.map((field) =>
              newColumn({
                accessor: (date: string) =>
                  getValueAtInstant(
                    (bracket as RateBracket)[field as keyof RateBracket],
                    date,
                  ),
                id: `${parameter.name}.${index}.${field}`,
                header: { shortTitle: $_(field, { locale: languageCode }) },
                unit: (parameter as unknown as { [key: string]: string })[
                  field === "base" ? "unit" : `${field}_unit`
                ],
              }),
            ),
          }),
        )
      } else {
        // Use a single column for each bracket, with a label stating "Entre x et y".

        const thresholdByRange = new Map<string, number | null>()
        const nonThresholdField = fields.filter(
          (field) => field !== "threshold",
        )[0]
        const valueByDateByRange: {
          [range: string]: { [date: string]: MaybeNumberValue | "empty" }
        } = {}
        for (const date of dates) {
          for (const [index, bracket] of parameter.brackets.entries()) {
            const minThreshold = getValueAtInstant(bracket.threshold, date)
            const nextBracket = parameter.brackets[index + 1]
            const maxThreshold =
              nextBracket === undefined
                ? undefined
                : getValueAtInstant(nextBracket.threshold, date)
            if (
              minThreshold === undefined ||
              minThreshold === "expected" ||
              maxThreshold === "expected"
            ) {
              continue
            }
            const valueAtDate = getValueAtInstant(
              (bracket as RateBracket)[nonThresholdField as keyof RateBracket],
              date,
            )
            if (valueAtDate === undefined || valueAtDate === "expected") {
              continue
            }

            const range = `${minThreshold.value ?? ""}-${maxThreshold?.value ?? ""}`
            if (thresholdByRange.get(range) === undefined) {
              thresholdByRange.set(range, minThreshold.value)
            }
            let valueByDate = valueByDateByRange[range]
            if (valueByDate == undefined) {
              valueByDate = valueByDateByRange[range] = {}
            }
            valueByDate[date] = valueAtDate
          }
        }

        const columnsRangeSorted = [...thresholdByRange.keys()].toSorted(
          (range1, range2) => {
            const [min1Text, max1Text] = range1.split("-")
            const [min2Text, max2Text] = range2.split("-")
            if (min1Text === "") {
              if (min2Text !== "") {
                return -1
              }
            } else if (min2Text === "") {
              return 1
            } else {
              const min1 = parseFloat(min1Text)
              const min2 = parseFloat(min2Text)
              if (min1 !== min2) {
                return min1 - min2
              }
            }
            if (max1Text === "") {
              return -1
            }
            if (max2Text === "") {
              return 1
            }
            const max1 = parseFloat(max1Text)
            const max2 = parseFloat(max2Text)
            return max1 - max2
          },
        )

        columns = columnsRangeSorted
          .filter((range) => {
            if (range.startsWith("-")) {
              return false
            }
            // Skip columns with constant zero value.
            const { constant, isConstant } = getConstantInfos(
              valueByDateByRange[range],
            )
            return !isConstant || (constant !== 0 && constant !== undefined)
          })
          .map((range) => {
            const threshold = thresholdByRange.get(range)!
            const valueByDate = valueByDateByRange[range]
            const latestInstant = getConstantInfos(valueByDate).latestInstant!
            const [minText, maxText] = range.split("-")
            // Note: Columns with minText === "" are already filtered.
            const min = parseFloat(minText)
            let max: number | undefined = undefined
            let shortTitle: string
            if (maxText === "") {
              if (
                threshold === 0 &&
                parameter.threshold_unit !== undefined &&
                ["PSS", "SMIC"].includes(parameter.threshold_unit)
              ) {
                shortTitle = "Sur tout salaire"
              } else {
                shortTitle = `Supérieur à ${formatCellNumber(
                  min,
                  unitByName,
                  parameter.threshold_unit,
                  latestInstant,
                  languageCode,
                )}`
              }
            } else {
              max = parseFloat(maxText)
              if (
                threshold === 0 &&
                parameter.threshold_unit !== undefined &&
                ["PSS", "SMIC"].includes(parameter.threshold_unit)
              ) {
                shortTitle = `Inférieur à ${formatCellNumber(
                  max,
                  unitByName,
                  parameter.threshold_unit,
                  latestInstant,
                  languageCode,
                )}`
              } else {
                shortTitle = `Entre ${formatCellNumber(
                  min,
                  unitByName,
                  parameter.threshold_unit,
                  latestInstant,
                  languageCode,
                )} et ${formatCellNumber(
                  max,
                  unitByName,
                  parameter.threshold_unit,
                  latestInstant,
                  languageCode,
                )}`
              }
            }

            return newColumn({
              accessor: (date: string) => valueByDate[date],
              header: { shortTitle },
              id: `${parameter.name}.${range}`,
              unit: (parameter as unknown as { [key: string]: string })[
                `${nonThresholdField}_unit`
              ],
            })
          })
      }

      return newColumn({
        columns,
        header,
        sourceUrl:
          editorUrl === undefined
            ? undefined
            : new URL(
                `${languageCode === defaultLanguageCode ? "" : `${languageCode}/`}${parameter.name}/edit`,
                editorUrl,
              ).toString(),
      })
    }

    case ParameterClass.Value: {
      switch (parameter.type) {
        case ValueType.Boolean:
        case ValueType.Number: {
          return newColumn({
            accessor: (date: string) =>
              getValueAtInstant(
                parameter.values as { [instant: string]: unknown },
                date,
              ),
            header,
            id: parameter.name,
            sourceUrl:
              editorUrl === undefined
                ? undefined
                : new URL(
                    `${languageCode === defaultLanguageCode ? "" : `${languageCode}/`}${parameter.name}/edit`,
                    editorUrl,
                  ).toString(),
            unit: parameter.unit,
          })
        }

        case ValueType.StringArray: {
          let maxLength = 0
          for (const value of Object.values(parameter.values)) {
            if (value.value !== null && value.value.length > maxLength) {
              maxLength = value.value.length
            }
          }

          const columns: ParametersTableColumn[] = []
          for (let index = 0; index < maxLength; index++) {
            columns.push(
              newColumn({
                accessor: (date: string) => {
                  const valueAtInstant = getValueAtInstant(
                    parameter.values,
                    date,
                  )
                  if (valueAtInstant === undefined) {
                    return undefined
                  }
                  return {
                    value:
                      valueAtInstant.value === null
                        ? null
                        : valueAtInstant.value[index],
                  }
                },
                header: { shortTitle: index.toString() },
                id: `${parameter.name}.${index}`,
              }),
            )
          }

          return newColumn({
            columns,
            header,
            sourceUrl:
              editorUrl === undefined
                ? undefined
                : new URL(
                    `${languageCode === defaultLanguageCode ? "" : `${languageCode}/`}${parameter.name}/edit`,
                    editorUrl,
                  ).toString(),
            unit: parameter.unit,
          })
        }

        case ValueType.StringByString: {
          const keys = new Set<string>()
          for (const value of Object.values(parameter.values)) {
            if (value.value !== null) {
              for (const key of Object.keys(value.value)) {
                keys.add(key)
              }
            }
          }

          const columns: ParametersTableColumn[] = [...keys].map((key) =>
            newColumn({
              accessor: (date: string) => {
                const valueAtInstant = getValueAtInstant(parameter.values, date)
                if (valueAtInstant === undefined) {
                  return undefined
                }
                return {
                  value:
                    valueAtInstant.value === null
                      ? null
                      : valueAtInstant.value[key],
                }
              },
              header: { shortTitle: key },
              id: `${parameter.name}.${key}`,
            }),
          )

          return newColumn({
            columns,
            header,
            sourceUrl:
              editorUrl === undefined
                ? undefined
                : new URL(
                    `${languageCode === defaultLanguageCode ? "" : `${languageCode}/`}${parameter.name}/edit`,
                    editorUrl,
                  ).toString(),
            unit: parameter.unit,
          })
        }

        default:
          assertNever("ValueParameter.type", parameter)
      }
    }

    default: {
      assertNever("Parameter.class", parameter)
    }
  }
}

export function buildParameterColumns({
  editorUrl,
  exclude,
  hiddenAncestors,
  incomeTaxYear,
  languageCode,
  parameter,
  unitByName,
  visibleAncestors,
}: {
  editorUrl?: string
  exclude: string[]
  hiddenAncestors: Array<NodeParameter & { children: undefined }>
  incomeTaxYear?: boolean
  languageCode: LanguageCode
  parameter: Parameter
  unitByName: { [name: string]: Unit }
  visibleAncestors: Array<NodeParameter & { children: undefined }>
}): {
  columns: ParametersTableColumn[]
  dates: string[]
  documentation?: string
} {
  const dates = [
    ...getParameterDates({
      exclude: exclude ?? [],
      parameter,
    }),
  ].toSorted((date1, date2) => date2.localeCompare(date1))

  // Generate documentationContext.
  const indexes = []
  let match
  const regexp = /\((\d{1,2})\)/g
  while ((match = regexp.exec(parameter.documentation ?? "")) !== null) {
    indexes.push(parseInt(match[1]))
  }
  const lastDocumentationIndex = indexes.reduce(
    (maxIndex, index) => Math.max(index, maxIndex),
    0,
  )
  const documentationContext: DocumentationContext = {
    fragmentByIndex: new Map(),
    nextIndex: lastDocumentationIndex + 1,
  }

  const column = buildParameterColumn({
    dates,
    documentationContext,
    editorUrl,
    exclude,
    hiddenAncestors,
    languageCode,
    parameter,
    unitByName,
    visibleAncestors,
  })

  const mergedMetadata = mergeMetadata(parameter) as Partial<Metadata>
  for (const [name, value] of Object.entries(mergedMetadata)) {
    if (Object.keys(value).length === 0) {
      delete mergedMetadata[name as keyof Metadata]
    }
  }

  return {
    columns: [
      newColumn({
        accessor: (date: string) => date,
        header: { shortTitle: $_("Date", { locale: languageCode }) },
        id: "date",
      }),
      ...(incomeTaxYear
        ? [
            newColumn({
              accessor: (date: string) =>
                (parseInt(date.split("-")[0]) + 1).toString(),
              header: {
                shortTitle: $_("Income tax year", { locale: languageCode }),
              },
              id: "raw",
            }),
          ]
        : []),
      column,
      ...buildMetaDataColumns(mergedMetadata, languageCode),
    ],

    dates,
    documentation:
      [...documentationContext.fragmentByIndex.entries()]
        .map(([index, fragment]) =>
          index > 0 ? `(${index}) ${fragment}` : fragment,
        )
        .join("\n\n") || undefined,
  }
}

export function computeMaxDepth(columns: ParametersTableColumn[]): number {
  return Math.max(
    ...columns.map((column) => {
      let depth = 1
      if (column.columns !== undefined) {
        depth += computeMaxDepth(column.columns)
      }
      return depth
    }),
  )
}

export function computeRowspan(
  column: ParametersTableColumn,
  depth: number,
  maxDepth: number,
): number {
  return column.columns === undefined ? maxDepth - depth + 1 : 1
}

function getConstantInfos(valueByInstant: {
  [instant: string]: MaybeNumberValue | "empty" | "expected"
}): { constant?: number; isConstant?: boolean; latestInstant?: string } {
  let constant: number | undefined = undefined
  let latestInstant: string | undefined = undefined
  for (const [instant, valueAtInstant] of Object.entries(valueByInstant)) {
    if (
      valueAtInstant === "empty" ||
      valueAtInstant === "expected" ||
      valueAtInstant.value === null
    ) {
      continue
    }
    if (latestInstant === undefined || latestInstant < instant) {
      latestInstant = instant
    }
    if (constant === undefined) {
      constant = valueAtInstant.value
      continue
    }
    if (valueAtInstant.value !== constant) {
      return {}
    }
  }
  // Note: constant & latestInstant may be undefined.
  return { constant, isConstant: true, latestInstant }
}

function getParameterDates({
  exclude,
  parameter,
}: {
  exclude: string[]
  parameter: Parameter
}): Set<string> {
  switch (parameter.class) {
    case ParameterClass.Node: {
      const dates = new Set<string>()
      for (const child of iterNodeParameterOrderedChildren(parameter)) {
        if (exclude.includes(child.id as string)) {
          continue
        }
        const childDates = getParameterDates({
          exclude: exclude.reduce((childExclude, partialName) => {
            const prefix = `${child.id}.`
            if (partialName.startsWith(prefix)) {
              childExclude.push(partialName.slice(prefix.length))
            }
            return childExclude
          }, [] as string[]),
          parameter: child,
        })
        for (const date of childDates) {
          dates.add(date)
        }
      }
      return dates
    }

    case ParameterClass.Scale: {
      let fields: Array<keyof AmountBracket> | Array<keyof RateBracket>
      const dates = new Set<string>()
      switch (parameter.type) {
        case ScaleType.LinearAverageRate:
        case ScaleType.MarginalRate: {
          fields = ["base", "threshold", "rate"] as Array<keyof RateBracket>
          for (const bracket of parameter.brackets) {
            for (const field of fields) {
              for (const [date, value] of Object.entries(
                bracket[field] ?? {},
              )) {
                if (value !== "expected") {
                  dates.add(date)
                }
              }
            }
          }
          return dates
        }

        case ScaleType.MarginalAmount:
        case ScaleType.SingleAmount: {
          fields = ["threshold", "amount"] as Array<keyof AmountBracket>
          const dates = new Set<string>()
          for (const bracket of parameter.brackets) {
            for (const field of fields) {
              for (const [date, value] of Object.entries(bracket[field])) {
                if (value !== "expected") {
                  dates.add(date)
                }
              }
            }
          }
          return dates
        }

        default: {
          assertNever("ScaleParameter.type", parameter)
        }
      }
    }

    case ParameterClass.Value: {
      return new Set(Object.keys(parameter.values))
    }

    default: {
      assertNever("Parameter.class", parameter)
    }
  }
}

function getValueAtInstant<T>(
  value: { [instant: string]: T } | undefined,
  instant: string,
): T | undefined {
  if (value === undefined) {
    return undefined
  }
  const foundInstantAndValue = Object.entries(value)
    .toSorted(([date1], [date2]) => date2.localeCompare(date1))
    .find(([instant1]) => instant1 <= instant)
  return foundInstantAndValue?.[1]
}

export function* iterRange(start: number, stop: number) {
  for (let i = start; i < stop; i++) {
    yield i
  }
}

function mergeMetadata(
  parameter: Parameter,
  mergedMetadata: Metadata = {
    notes: {},
    official_journal_date: {},
    reference: {},
  },
): Metadata {
  for (const [name, mergedValuesByDate] of Object.entries(mergedMetadata)) {
    const valueByDate = parameter[name as keyof Parameter] as
      | OfficialJournalDatesByInstant
      | ReferencesByInstant
    if (valueByDate !== undefined) {
      for (const [date, value] of Object.entries(valueByDate)) {
        if (name === "official_journal_date") {
          const mergedValues = (mergedValuesByDate[date] =
            mergedValuesByDate[date] || []) as string[]
          const typedValue = value as string | null
          if (typedValue !== null) {
            if (!mergedValues.includes(typedValue)) {
              mergedValues.push(typedValue)
            }
          }
        } else {
          const mergedValues = (mergedValuesByDate[date] =
            mergedValuesByDate[date] || []) as Reference[]
          for (const typedValue of value as Reference[]) {
            if (
              mergedValues.find((mergedValue) => {
                if (
                  Object.keys(mergedValue).length !==
                  Object.keys(typedValue).length
                ) {
                  return false
                }
                return Object.entries(mergedValue).every(
                  ([itemName, itemValue]) =>
                    itemValue === typedValue[itemName as keyof Reference],
                )
              }) === undefined
            ) {
              mergedValues.push(typedValue)
            }
          }
        }
      }
    }
  }
  if (
    parameter.class === ParameterClass.Node &&
    parameter.children !== undefined
  ) {
    for (const child of Object.values(parameter.children)) {
      mergeMetadata(child, mergedMetadata)
    }
  }
  return mergedMetadata
}

function newColumn(column: ParametersTableColumn): ParametersTableColumn {
  return {
    ...column,
    colspan:
      column.columns === undefined
        ? undefined
        : column.columns.reduce(
            (sum, column) => sum + (column.colspan ?? 1),
            0,
          ),
  }
}

export function* walkParameterColumns(
  columns: ParametersTableColumn[],
  depth: number = 1,
): Generator<{ column: ParametersTableColumn; depth: number }, void, unknown> {
  for (const column of columns) {
    yield { column, depth }
    if (column.columns !== undefined) {
      yield* walkParameterColumns(column.columns, depth + 1)
    }
  }
}

export function walkToTableOptions(
  tableOfContentsItem: TableOfContentsItem,
  parameterName: string,
  incomeTaxYear?: boolean,
): { exclude?: string[]; income_tax_year?: boolean } | undefined {
  if (tableOfContentsItem.subparams !== undefined) {
    for (const tableOfContentsItemSubparam of tableOfContentsItem.subparams) {
      if (
        (tableOfContentsItemSubparam as TableOfContentsItem).title !== undefined
      ) {
        const tableOptions = walkToTableOptions(
          tableOfContentsItemSubparam as TableOfContentsItem,
          parameterName,
          (tableOfContentsItemSubparam as TableOfContentsItem)
            .income_tax_year ?? incomeTaxYear,
        )
        if (tableOptions !== undefined) {
          return tableOptions
        }
      } else if (
        (tableOfContentsItemSubparam as TableOfContentsItemSubparamSubsection)
          .subsection !== undefined
      ) {
        const { income_tax_year, subsection } =
          tableOfContentsItemSubparam as TableOfContentsItemSubparamSubsection
        if (
          subsection === parameterName ||
          parameterName.startsWith(subsection + ".")
        ) {
          return {
            income_tax_year: income_tax_year ?? incomeTaxYear,
          }
        }
      } else {
        const { exclude, income_tax_year, table } =
          tableOfContentsItemSubparam as TableOfContentsItemSubparamTable
        if (table === parameterName) {
          return {
            exclude: exclude,
            income_tax_year: income_tax_year ?? incomeTaxYear,
          }
        }
      }
    }
  } else if (
    tableOfContentsItem.subsection !== undefined &&
    (tableOfContentsItem.subsection === parameterName ||
      parameterName.startsWith(tableOfContentsItem.subsection + "."))
  ) {
    return {
      income_tax_year: tableOfContentsItem.income_tax_year ?? incomeTaxYear,
    }
  }
  return undefined
}
