import type { Parameter } from "@tax-benefit/openfisca-json-model"

import { assertNever } from "$lib/asserts"

export type ForgeType = (typeof forgeTypes)[number]

export interface RepositoryConfig {
  accessToken?: string // Only used by editor
  authorName: string
  authorEmail: string
  branch: string
  forgeDomainName: string
  forgeType: ForgeType
  group: string
  parametersDir: string
  project: string
}

export const forgeTypes = ["GitHub", "GitLab"] as const

export function newParameterRepositoryRawUrl(
  { branch, forgeDomainName, forgeType, group, project }: RepositoryConfig,
  parameter: Parameter,
): string | undefined {
  const path = parameter.file_path
  if (path === undefined) {
    return undefined
  }
  switch (forgeType) {
    case "GitHub": {
      return `https://raw.githubusercontent.com/${group}/${project}/refs/heads/${branch}/${path}`
    }

    case "GitLab": {
      return `https://${forgeDomainName}/${group}/${project}/-/raw/${branch}/${path}`
    }

    default: {
      assertNever("newParameterRepositoryUrl: forgeType", forgeType)
    }
  }
}

export function newParameterRepositoryUrl(
  { branch, forgeDomainName, forgeType, group, project }: RepositoryConfig,
  parameter: Parameter,
): string | undefined {
  const path = parameter.file_path
  if (path === undefined) {
    return undefined
  }
  switch (forgeType) {
    case "GitHub": {
      return `https://${forgeDomainName}/${group}/${project}/tree/${branch}/${path}`
    }

    case "GitLab": {
      return `https://${forgeDomainName}/${group}/${project}/-/blob/${branch}/${path}`
    }

    default: {
      assertNever("newParameterRepositoryUrl: forgeType", forgeType)
    }
  }
}
