<script lang="ts">
  import {
    ParameterClass,
    ScaleType,
    type AmountBracket,
    type MaybeBooleanValue,
    type NodeParameter,
    type NumberValue,
    type Parameter,
    type RateBracket,
    type Reference,
    type ValueParameter,
  } from "@tax-benefit/openfisca-json-model"
  import { _ } from "svelte-i18n"

  import { page } from "$app/stores"
  import { assertNever } from "$lib/asserts"
  import { formatCellNumber } from "$lib/formatters"
  import type { LanguageCode } from "$lib/i18n"
  import { getParameterLongTitle } from "$lib/parameters"
  import {
    buildParameterColumns,
    computeMaxDepth,
    computeRowspan,
    iterRange,
    walkParameterColumns,
  } from "$lib/parameters_tables"
  import type { TableOfContentsItem } from "$lib/table_of_contents"
  import { newParameterUrlString } from "$lib/urls"

  import ParameterBreadcrumb from "./ParameterBreadcrumb.svelte"

  let {
    embeddedParameter,
    exclude,
    hiddenAncestors,
    incomeTaxYear,
    languageCode,
    parameter,
    tableOfContentsItem,
    tableOfContentsItemId,
    visibleAncestors,
  }: {
    embeddedParameter: Parameter
    exclude?: string[]
    hiddenAncestors: Array<NodeParameter & { children: undefined }>
    incomeTaxYear?: boolean
    languageCode: LanguageCode
    parameter: Parameter
    tableOfContentsItem?: TableOfContentsItem
    tableOfContentsItemId?: string
    visibleAncestors: Array<NodeParameter & { children: undefined }>
  } = $props()
  let { data } = $derived($page)
  let dateFormatter = $state(
    new Intl.DateTimeFormat(languageCode, { dateStyle: "short" }),
  )
  let {
    customization,
    dbnomics,
    editorUrl,
    exportCsv,
    exportJson,
    exportXlsx,
    unitByName,
  } = $derived(data)
  let { columns, dates, documentation } = $derived(
    buildParameterColumns({
      editorUrl,
      exclude: exclude ?? [],
      hiddenAncestors,
      incomeTaxYear,
      languageCode,
      parameter: embeddedParameter,
      unitByName,
      visibleAncestors,
    }),
  )
  let maxDepth = $derived(computeMaxDepth(columns))

  function* walkDbnomicsSeriesCode(
    embeddedParameter: Parameter,
  ): Generator<string, void, unknown> {
    switch (embeddedParameter.class) {
      case ParameterClass.Node: {
        for (const child of Object.values(embeddedParameter.children ?? {})) {
          yield* walkDbnomicsSeriesCode(child)
        }
        return
      }

      case ParameterClass.Scale: {
        let fields: Array<keyof AmountBracket> | Array<keyof RateBracket>
        switch (embeddedParameter.type) {
          case ScaleType.LinearAverageRate:
          case ScaleType.MarginalRate: {
            fields = ["base", "threshold", "rate"] as Array<keyof RateBracket>

            let hasBase = embeddedParameter.brackets.some(
              (bracket) => bracket.base !== undefined,
            )
            if (!hasBase) {
              fields.splice(0, 1) // Remove "base" field.
            }

            break
          }

          case ScaleType.MarginalAmount:
          case ScaleType.SingleAmount: {
            fields = ["threshold", "amount"] as Array<keyof AmountBracket>

            break
          }

          default: {
            assertNever("ScaleParameter.type", embeddedParameter)
          }
        }

        for (const bracketIndex of embeddedParameter.brackets.keys()) {
          for (const field of fields) {
            yield `${embeddedParameter.name}.${bracketIndex}.${field}`
          }
        }

        return
      }

      case ParameterClass.Value: {
        yield embeddedParameter.name as string
        return
      }

      default: {
        assertNever("Parameter.class", embeddedParameter)
      }
    }
  }
</script>

<ParameterBreadcrumb
  {customization}
  {embeddedParameter}
  {languageCode}
  {tableOfContentsItem}
  {tableOfContentsItemId}
  {visibleAncestors}
/>

<div class="float-right">
  {#if customization !== "ipp"}
    <a
      class="text-[#008a9b] underline"
      href={newParameterUrlString({
        languageCode,
        name: parameter.name,
      })}>Arborescence</a
    >
  {/if}
  {#if exportCsv}
    <a
      class="text-[#008a9b] underline"
      download="{parameter.title}.csv"
      href={newParameterUrlString({
        action: "csv",
        languageCode,
        name: parameter.name,
      })}>CSV</a
    >
  {/if}
  {#if dbnomics !== undefined}
    <a
      class="text-[#008a9b] underline"
      href={new URL(
        `/series?${new URLSearchParams({
          series_ids: [...walkDbnomicsSeriesCode(embeddedParameter)]
            .map(
              (seriesCode) =>
                `${dbnomics.providerCode}/${dbnomics.datasetCode}/${seriesCode}`,
            )
            .join(","),
        })}`,
        dbnomics.url,
      ).toString()}
      target="_blank">DBnomics</a
    >
  {/if}
  {#if exportJson}
    <a
      class="text-[#008a9b] underline"
      download="{parameter.title}.json"
      href={newParameterUrlString({
        action: "json",
        languageCode,
        name: parameter.name,
      })}>JSON</a
    >
  {/if}
  {#if exportXlsx}
    <a
      class="text-[#008a9b] underline"
      download="{parameter.title}.xlsx"
      href={newParameterUrlString({
        action: "xlsx",
        languageCode,
        name: parameter.name,
      })}>XLSX</a
    >
  {/if}
</div>

<h1
  class="mb-5 border-b-2 border-solid border-[#005165] text-[1.143em] font-bold leading-[1.3em]"
>
  <span class="inline-block bg-[#005165] px-2 py-1 tracking-wider text-white"
    >{getParameterLongTitle(embeddedParameter, languageCode) ??
      embeddedParameter.id ??
      "Paramètres"}</span
  >
</h1>

<div
  class:overflow-auto={customization === "ipp"}
  class:max-h-[600px]={customization === "ipp"}
>
  <table class="w-full border-collapse">
    <thead class="sticky top-0 z-40 border-b-2 border-solid border-gray-100">
      {#each iterRange(1, maxDepth + 1) as currentDepth}
        <tr class="odd:bg-gray-200 even:bg-gray-100">
          {#each walkParameterColumns(columns) as { column, depth }}
            {#if depth === currentDepth}
              {@const { header } = column}
              <th
                class="00 border border-solid border-gray-100 p-1 text-center"
                class:sticky={column.id === "date"}
                class:left-0={column.id === "date"}
                class:top-0={column.id === "date"}
                class:gray-100={column.id === "date"}
                class:bg-gray-200={column.id === "date"}
                class:z-50={column.id === "date"}
                colspan={column.colspan}
                id={column.id === "date" ? column.id : null}
                rowspan={computeRowspan(column, depth, maxDepth)}
                style:flex="{column.width ?? 1} 0 auto"
                style:min-width="{(column.width ?? 1) * 100}px"
              >
                {#if column.sourceUrl !== undefined}
                  <span class="edit-link">
                    {#if header.longTitle === undefined}
                      {header.shortTitle}
                    {:else}
                      <abbr title={header.longTitle}>
                        {header.shortTitle}
                      </abbr>
                    {/if}
                    <br />
                    <a
                      class="text-[#008a9b] underline"
                      href={column.sourceUrl}
                      target="_blank"
                    >
                      Edit
                    </a>
                  </span>
                {:else if header.longTitle === undefined}
                  {header.shortTitle}
                {:else}
                  <abbr title={header.longTitle}>
                    {header.shortTitle}
                  </abbr>
                {/if}
              </th>
            {/if}
          {/each}
        </tr>
      {/each}
    </thead>
    <tbody>
      {#each dates as date}
        <tr class="odd:bg-gray-50">
          {#each walkParameterColumns(columns) as { column }}
            {#if column.id !== undefined}
              {@const valueAtInstant = column.accessor?.(date)}
              <td
                class="border border-solid border-gray-100 p-1 text-center first:sticky first:left-0 first:z-30 first:bg-gray-200"
                colspan={column.colspan}
                style:flex="{column.width ?? 1} 0 auto"
              >
                {#if valueAtInstant != null}
                  {#if column.id === "date"}
                    {dateFormatter.format(new Date(valueAtInstant as string))}
                  {:else if ["notes", "reference"].includes(column.id ?? "")}
                    {@const references = valueAtInstant as Reference[]}
                    {#each references as reference}
                      <div>
                        {#if reference.href === undefined}
                          {reference.title}
                        {:else}
                          <a
                            class="text-[#008a9b] underline"
                            href={reference.href}
                            target="_blank"
                          >
                            {reference.title || reference.href}
                          </a>
                        {/if}
                      </div>
                    {/each}
                  {:else if column.id === "official_journal_date"}
                    {#each valueAtInstant as string[] as item}
                      <div>{item}</div>
                    {/each}
                  {:else if column.id === "raw"}
                    {valueAtInstant}
                  {:else if valueAtInstant === "expected"}
                    <i>{$_("expected")}</i>
                  {:else if valueAtInstant === "empty" || (valueAtInstant as NumberValue).value === null}
                    {""}
                  {:else if typeof (valueAtInstant as MaybeBooleanValue).value === "boolean"}
                    <i
                      >{(valueAtInstant as MaybeBooleanValue).value
                        ? $_("True")
                        : $_("False")}</i
                    >
                  {:else if typeof (valueAtInstant as NumberValue).value === "number"}
                    {formatCellNumber(
                      (valueAtInstant as NumberValue).value,
                      unitByName,
                      column.unit,
                      date,
                      languageCode,
                    )}
                  {:else}
                    {(valueAtInstant as NumberValue).value ?? ""} ({(
                      parameter as ValueParameter
                    )?.type}
                    {JSON.stringify(valueAtInstant)})
                  {/if}
                {/if}
              </td>
            {/if}
          {/each}
        </tr>
      {/each}
    </tbody>
  </table>
</div>
{#if documentation !== undefined}
  <p class="mt-4">
    {#each documentation.split("\n") as line, index}
      {#if index > 0}<br />{/if}
      {line}
    {/each}
  </p>
{/if}
