import { base } from "$app/paths"
import { tableOfContentsItemIdByParameterNameByLanguageCode } from "$lib/data/table_of_contents.json"
import { defaultLanguageCode, type LanguageCode } from "$lib/i18n"

export function newParameterUrlString({
  action,
  languageCode,
  name,
}: {
  action?: "csv" | "json" | "table" | "xlsx"
  languageCode: LanguageCode
  name?: string
}) {
  if (!name || tableOfContentsItemIdByParameterNameByLanguageCode == null) {
    return `${base}${languageCode === defaultLanguageCode ? "" : `/${languageCode}`}/parameters${name ? `/${name}` : ""}${action === undefined ? "" : `/${action}`}`
  }
  const nameSplit = name.split(".")
  const tableOfContentsItemIdByParameterName =
    tableOfContentsItemIdByParameterNameByLanguageCode[languageCode]
  for (let i = nameSplit.length; i > 0; i--) {
    const ancestorName = nameSplit.slice(0, i).join(".")
    const tableOfContentsItemId = (
      tableOfContentsItemIdByParameterName as {
        [parameterName: string]: string
      }
    )[ancestorName]
    if (tableOfContentsItemId !== undefined) {
      return `${base}${languageCode === defaultLanguageCode ? "" : `/${languageCode}`}/parameters/${tableOfContentsItemId}${name ? `/${name}` : ""}${action === undefined ? "" : `/${action}`}`
    }
  }
  console.error(`Parameter "${name}" not found in table of contents items`)
  return `${base}${languageCode === defaultLanguageCode ? "" : `/${languageCode}`}/parameters${name ? `/${name}` : ""}${action === undefined ? "" : `/${action}`}`
}

export function newParametersUrlString({
  languageCode,
}: {
  languageCode: LanguageCode
}) {
  return `${base}${languageCode === defaultLanguageCode ? "" : `/${languageCode}`}/parameters`
}

export function newTableOfContentsItemIdUrlString({
  languageCode,
  tableOfContentsItemId,
}: {
  languageCode: LanguageCode
  tableOfContentsItemId?: string
}) {
  return `${base}${languageCode === defaultLanguageCode ? "" : `/${languageCode}`}/parameters${tableOfContentsItemId === undefined ? "" : `/${tableOfContentsItemId}`}`
}
