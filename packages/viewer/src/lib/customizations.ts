export type Customization = (typeof customizations)[number]

export const customizations = ["ipp", "leximpact", "openfisca"] as const
