import {
  ParameterClass,
  scaleByInstantFromBrackets,
  ScaleType,
  ValueType,
  type MaybeNumberValue,
  type NodeParameter,
  type Parameter,
  type Reference,
  type ScaleAtInstant,
  type ScaleParameter,
  type ValueAtInstant,
  type ValueParameter,
} from "@tax-benefit/openfisca-json-model"

import { assertNever } from "$lib/asserts"
import type { LanguageCode } from "$lib/i18n"

export interface InstantReferencesAndScale {
  instant: string
  references: Reference[]
  scaleAtInstant?: ScaleAtInstant
}

export interface InstantReferencesAndValue {
  instant: string
  references: Reference[]
  valueAtInstant?: ValueAtInstant
}

export type ParameterViewOptions = {
  depth: number
  exclude?: string[]
  flat?: boolean
  maxDepth: number
}

export function buildInstantReferencesAndScaleArray(
  parameter: ScaleParameter,
): InstantReferencesAndScale[] {
  const scaleByInstant = scaleByInstantFromBrackets(parameter.brackets ?? [])
  const instantReferencesAndScaleByInstant: {
    [instant: string]: InstantReferencesAndScale
  } = Object.fromEntries(
    Object.entries(scaleByInstant).map(([instant, scaleAtInstant]) => [
      instant,
      { instant, references: [], scaleAtInstant },
    ]),
  )
  if (parameter.reference !== undefined) {
    for (const [instant, references] of Object.entries(parameter.reference)) {
      if (instantReferencesAndScaleByInstant[instant] === undefined) {
        instantReferencesAndScaleByInstant[instant] = {
          instant,
          references,
        }
      } else {
        instantReferencesAndScaleByInstant[instant].references = references
      }
    }
  }
  return Object.entries(instantReferencesAndScaleByInstant)
    .sort(([instant1], [instant2]) => instant2.localeCompare(instant1))
    .map(([, instantReferencesAndScale]) => instantReferencesAndScale)
}

export function buildInstantReferencesAndValueArray(
  parameter: ValueParameter,
): InstantReferencesAndValue[] {
  const instantReferencesAndValueByInstant: {
    [instant: string]: InstantReferencesAndValue
  } = Object.fromEntries(
    Object.entries(parameter.values ?? {}).map(([instant, valueAtInstant]) => [
      instant,
      { instant, references: [], valueAtInstant },
    ]),
  )
  if (parameter.reference !== undefined) {
    for (const [instant, references] of Object.entries(parameter.reference)) {
      if (instantReferencesAndValueByInstant[instant] === undefined) {
        instantReferencesAndValueByInstant[instant] = {
          instant,
          references,
        }
      } else {
        instantReferencesAndValueByInstant[instant].references = references
      }
    }
  }
  return Object.entries(instantReferencesAndValueByInstant)
    .sort(([instant1], [instant2]) => instant2.localeCompare(instant1))
    .map(([, instantReferencesAndValue]) => instantReferencesAndValue)
}

export function countParameterColumnsApproximation(
  parameter: Parameter,
): number {
  switch (parameter.class) {
    case ParameterClass.Node: {
      return parameter.children === undefined
        ? 0
        : Object.values(parameter.children).reduce(
            (count, child) => count + countParameterColumnsApproximation(child),
            0,
          )
    }

    case ParameterClass.Scale: {
      let fields: string[]
      switch (parameter.type) {
        case ScaleType.LinearAverageRate:
        case ScaleType.MarginalRate: {
          fields = ["base", "threshold", "rate"]

          let hasBase = false
          for (const bracket of parameter.brackets) {
            if (bracket.base !== undefined) {
              hasBase = true
            }
          }
          if (!hasBase) {
            fields.splice(0, 1) // Remove "base" field.
          }
          break
        }

        case ScaleType.MarginalAmount:
        case ScaleType.SingleAmount: {
          fields = ["threshold", "amount"]
          break
        }

        default: {
          assertNever("ScaleParameter.type", parameter)
        }
      }
      return fields.length
    }

    case ParameterClass.Value: {
      return 1
    }

    default: {
      assertNever("Parameter.class", parameter)
    }
  }
}

export function getEmbeddedParameter(
  rootParameter: Parameter,
  name?: string,
): {
  ancestors: Array<NodeParameter & { children: undefined }>
  embeddedParameter: Parameter | undefined
} {
  if (name === undefined || name === "") {
    return { ancestors: [], embeddedParameter: rootParameter }
  }
  const ancestors: Array<NodeParameter & { children: undefined }> = []
  let embeddedParameter = rootParameter
  for (const id of name.split(".")) {
    const parent = { ...embeddedParameter } as NodeParameter & {
      children: undefined
    }
    delete parent.children
    ancestors.push(parent)

    const children =
      embeddedParameter.class === ParameterClass.Node
        ? embeddedParameter.children
        : undefined
    if (children === undefined) {
      return { ancestors, embeddedParameter: undefined }
    }
    embeddedParameter = children[id]
    if (embeddedParameter === undefined) {
      return { ancestors, embeddedParameter: undefined }
    }
  }
  return { ancestors, embeddedParameter }
}

export function getEmbeddedParameterBoundingYears(
  embeddedParameter: Parameter,
): { firstYear?: number; lastYear?: number } {
  let firstYear: number | undefined = undefined
  let lastYear: number | undefined = undefined
  switch (embeddedParameter.class) {
    case ParameterClass.Node: {
      firstYear = 9999
      lastYear = 1
      for (const child of Object.values(embeddedParameter.children ?? {})) {
        const childBoundingYears = getEmbeddedParameterBoundingYears(child)
        if ((childBoundingYears.firstYear ?? 9999) < (firstYear ?? 9999)) {
          firstYear = childBoundingYears.firstYear
        }
        if (
          lastYear !== undefined &&
          (childBoundingYears.lastYear === undefined ||
            childBoundingYears.lastYear > lastYear)
        ) {
          lastYear = childBoundingYears.lastYear
        }
      }
      break
    }

    case ParameterClass.Scale: {
      let fields: string[]
      switch (embeddedParameter.type) {
        case ScaleType.LinearAverageRate:
        case ScaleType.MarginalRate: {
          fields = ["base", "threshold", "rate"]
          break
        }

        case ScaleType.MarginalAmount:
        case ScaleType.SingleAmount: {
          fields = ["threshold", "amount"]
          break
        }

        default: {
          assertNever("ScaleParameter.type", embeddedParameter)
        }
      }

      let firstDate: string | undefined = undefined
      let lastDate: string | undefined = undefined
      for (const bracket of embeddedParameter.brackets) {
        for (const field of fields) {
          const valueByDate = (
            bracket as unknown as {
              [field: string]: {
                [instant: string]: MaybeNumberValue | "expected"
              }
            }
          )[field]
          if (valueByDate !== undefined) {
            for (const [date, value] of Object.entries(valueByDate)) {
              if (value === "expected") {
                continue
              }
              if (value.value !== null && date < (firstDate ?? "9999-12-31")) {
                firstDate = date
              }
              if (value.value === null && date > (lastDate ?? "0001-01-01")) {
                lastDate = date
              }
            }
          }
        }
      }

      firstYear =
        firstDate === undefined ? undefined : parseInt(firstDate.split("-")[0])
      lastYear =
        lastDate === undefined
          ? undefined
          : lastDate.endsWith("-01-01")
            ? parseInt(lastDate.split("-")[0]) - 1
            : parseInt(lastDate.split("-")[0])
      break
    }

    case ParameterClass.Value: {
      let firstDate: string | undefined = undefined
      let lastDate: string | undefined = undefined
      for (const [date, value] of Object.entries(embeddedParameter.values)) {
        if (value === "expected") {
          continue
        }
        if (value.value !== null && date < (firstDate ?? "9999-12-31")) {
          firstDate = date
        }
        if (value.value === null && date > (lastDate ?? "0001-01-01")) {
          lastDate = date
        }
      }
      firstYear =
        firstDate === undefined ? undefined : parseInt(firstDate.split("-")[0])
      lastYear =
        lastDate === undefined
          ? undefined
          : lastDate.endsWith("-01-01")
            ? parseInt(lastDate.split("-")[0]) - 1
            : parseInt(lastDate.split("-")[0])
      break
    }

    default: {
      assertNever("Parameter.class", embeddedParameter)
    }
  }
  return { firstYear, lastYear }
}

export function getParameterLongTitle(
  parameter: Parameter,
  languageCode: LanguageCode,
): string | undefined {
  let title =
    languageCode === "en"
      ? (parameter.label_en ?? parameter.description)
      : parameter.description
  if (title === undefined) {
    title =
      languageCode === "en"
        ? (parameter.short_label_en ?? parameter.short_label)
        : parameter.short_label
  }
  return title
}

export function getParameterShortTitle(
  parameter: Parameter,
  languageCode: LanguageCode,
): string | undefined {
  let title =
    languageCode === "en"
      ? (parameter.short_label_en ?? parameter.short_label)
      : parameter.short_label
  if (title === undefined) {
    title =
      languageCode === "en"
        ? (parameter.label_en ?? parameter.description)
        : parameter.description
  }
  return title
}

export function* iterNodeParameterOrderedChildren(
  node: NodeParameter,
): Generator<Parameter, void, unknown> {
  if (node.children !== undefined) {
    if (node.order === undefined) {
      for (const child of Object.values(node.children)) {
        yield child
      }
    } else {
      for (const childName of node.order) {
        const child = node.children[childName]
        if (child !== undefined) {
          yield child
        }
      }
      for (const [childName, child] of Object.entries(node.children)) {
        if (!node.order.includes(childName)) {
          yield child
        }
      }
    }
  }
}

export function labelFromParameterClass(
  parameterClass: ParameterClass | string,
): string {
  return (
    {
      [ParameterClass.Node]: "Nœud",
      [ParameterClass.Scale]: "Barème",
      [ParameterClass.Value]: "Valeur",
    }[parameterClass] ?? parameterClass
  )
}

export function labelFromScaleType(type: ScaleType | string): string {
  return (
    {
      [ScaleType.LinearAverageRate]: "à taux moyen linéaire",
      [ScaleType.MarginalAmount]: "à montant marginal",
      [ScaleType.MarginalRate]: "à taux marginal",
      [ScaleType.SingleAmount]: "à montant unique",
    }[type] ?? type
  )
}

export function labelFromValueType(type: ValueType | string): string {
  return (
    {
      [ValueType.Boolean]: "booléen",
      [ValueType.Number]: "nombre",
      [ValueType.StringArray]: "tableau de chaînes de caractères",
      [ValueType.StringByString]: "dictionnaire de chaines de caractères",
    }[type] ?? type
  )
}
