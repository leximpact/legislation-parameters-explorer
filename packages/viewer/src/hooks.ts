import type { Handle } from "@sveltejs/kit"

import {
  allLanguageCodes,
  defaultLanguageCode,
  type LanguageCode,
} from "$lib/i18n"

export const handle: Handle = async ({ event, resolve }) => {
  const pathname = event.url.pathname
  const match = pathname.match(/^\/([a-z]{2})(\/|$)/)
  const languageCode =
    match === null
      ? defaultLanguageCode
      : allLanguageCodes.includes(match[1] as LanguageCode)
        ? (match[1] as LanguageCode)
        : defaultLanguageCode
  return resolve(event, {
    transformPageChunk: ({ html }) => html.replace("%lang%", languageCode),
  })
}
