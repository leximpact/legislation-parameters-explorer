import fs from "fs-extra"

import {
  tableOfContents,
  walkTableOfContentsItemParametersName,
} from "$lib/server/table_of_contents_without_data"

export {} // Make this file a module to be able to use top-level awaits.

const { tableOfContentsItemByIdByLanguageCode } = tableOfContents ?? {}

const tableOfContentsItemsIdByLanguageCode = Object.fromEntries(
  Object.entries(tableOfContentsItemByIdByLanguageCode ?? {}).map(
    ([languageCode, tableOfContentsItemById]) => [
      languageCode,
      Object.keys(tableOfContentsItemById),
    ],
  ),
)

let tableOfContentsItemIdByParameterNameByLanguageCode:
  | { [languageCode: string]: { [parameterName: string]: string } }
  | undefined = undefined
if (tableOfContents !== undefined) {
  tableOfContentsItemIdByParameterNameByLanguageCode = {}
  for (const [languageCode, tableOfContentsItemById] of Object.entries(
    tableOfContents.tableOfContentsItemByIdByLanguageCode,
  )) {
    const tableOfContentsItemIdByParameterName: {
      [parameterName: string]: string
    } = (tableOfContentsItemIdByParameterNameByLanguageCode[languageCode] = {})
    for (const [itemId, tableOfContentsItem] of Object.entries(
      tableOfContentsItemById,
    )) {
      for (const parameterName of walkTableOfContentsItemParametersName(
        tableOfContentsItem,
      )) {
        tableOfContentsItemIdByParameterName[parameterName] = itemId
      }
    }
  }
}

await fs.ensureDir("src/lib/data")
await fs.writeJson(
  "src/lib/data/table_of_contents.json",
  {
    tableOfContentsItemIdByParameterNameByLanguageCode:
      tableOfContentsItemIdByParameterNameByLanguageCode ?? null,
    tableOfContentsItemsIdByLanguageCode,
  },
  { spaces: 2 },
)
