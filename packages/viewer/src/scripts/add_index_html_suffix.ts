import fs from "fs-extra"
import { JSDOM } from "jsdom"
import path from "path"

import { walkDir } from "$lib/server/file_systems"

const buildDir = "build"
for (const relativeSplitPath of walkDir(buildDir)) {
  const filename = relativeSplitPath.at(-1) as string
  if (filename.match(/^.*\.html$/) === null) {
    continue
  }
  const filePath = path.join(buildDir, ...relativeSplitPath)
  const html = await fs.readFile(filePath, {
    encoding: "utf-8",
  })
  const dom = new JSDOM(html)
  const { body, head } = dom.window.document

  // Rewrite redirections.
  for (const scriptElement of head.querySelectorAll("script")) {
    if (scriptElement.textContent !== null) {
      scriptElement.textContent = scriptElement.textContent.replace(
        /location\.href="(.*?)\/?";/g,
        'location.href="$1/index.html";',
      )
    }
  }
  for (const metaElement of head.querySelectorAll(
    "meta[http-equiv='refresh']",
  )) {
    const content = metaElement.getAttribute("content")
    if (content !== null) {
      metaElement.setAttribute(
        "content",
        content.replace(/url=(.*)\/?$/, "url=$1/index.html"),
      )
    }
  }

  // Rewrite links.
  for (const aElement of body.querySelectorAll("a[href]")) {
    const href = aElement.getAttribute("href") as string
    if (
      href.startsWith("/") &&
      !href.endsWith("/csv") &&
      !href.endsWith("/doi") &&
      !href.endsWith("/json") &&
      !href.endsWith("/xlsx")
    ) {
      aElement.setAttribute("href", href.replace(/^(.*)\/?$/, "$1/index.html"))
    }
  }

  await fs.writeFile(filePath, dom.serialize(), { encoding: "utf-8" })
}
