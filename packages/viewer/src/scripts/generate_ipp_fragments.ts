import fs from "fs-extra"
import { JSDOM } from "jsdom"
import path from "path"

// Note: Don't use dedent to generate fragments because they convert \n to newlines!

import { walkDir } from "$lib/server/file_systems"

const buildDir = "build"
for (const relativeSplitPath of walkDir(buildDir)) {
  const filename = relativeSplitPath.at(-1) as string
  if (filename.match(/^.*\.html$/) === null) {
    continue
  }
  const filenameCore = filename.replace(/\.html$/, "")
  const filePath = path.join(buildDir, ...relativeSplitPath)
  const html = await fs.readFile(filePath, {
    encoding: "utf-8",
  })
  const dom = new JSDOM(html)
  const { body, head } = dom.window.document

  for (const aElement of body.querySelectorAll("a[href]")) {
    const href = aElement.getAttribute("href") as string
    const hrefUpdated = href
      .replace(
        /^(\/(en\/)?parameters\/.*\/(csv|json|xlsx))\/?$/,
        `/wp-content/themes/ipp/baremes-ipp$1`,
      )
      .replace(/^\/en\/parameters(\/.*|$)/, `/en/ipp-tax-and-benefit-tables$1`)
      .replace(/^\/parameters(\/.*|$)/, `/baremes-ipp$1`)
    if (hrefUpdated != href) {
      aElement.setAttribute("href", hrefUpdated)
    }
  }

  const sectionElement = body.querySelector(
    ":scope > div#legislation-parameters-explorer-embedded > section.bg-white",
  )
  if (sectionElement?.querySelector(":scope > div.float-right") === null) {
    // Table of contents page
    // => Remove breadcrumb & heading.
    sectionElement?.querySelector(":scope > ul#breadcrumb")?.remove()
    sectionElement?.querySelector(":scope > h1")?.remove()
  }

  const fragment = `
    ${[
      ...head.querySelectorAll(`link[rel="stylesheet"]`),
      ...head.querySelectorAll(`link[rel="modulepreload"]`),
    ]
      .map((element) => element.outerHTML)
      .join("\n")}
    ${body.innerHTML}
  `
  await fs.writeFile(
    path.join(
      buildDir,
      ...relativeSplitPath.slice(0, -1),
      `${filenameCore}.fragment.html`,
    ),
    fragment,
    { encoding: "utf-8" },
  )
  await fs.remove(filePath)
}
