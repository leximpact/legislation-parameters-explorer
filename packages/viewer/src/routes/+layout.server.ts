import { execSync } from "child_process"

import { dev } from "$app/environment"
import config from "$lib/server/config"
import { unitByName, units } from "$lib/server/units"

import type { LayoutServerLoad } from "./$types"

export const csr = dev
export const prerender = true
export const trailingSlash = "always"

const {
  customization,
  dbnomics,
  editorUrl,
  exportCsv,
  exportJson,
  exportXlsx,
  parametersDir,
  parametersRepository,
  title,
} = config
const commit = execSync("git rev-parse HEAD", {
  cwd: parametersDir,
  env: process.env,
  encoding: "utf-8",
  stdio: ["ignore", "pipe", "pipe"],
})

export const load: LayoutServerLoad = () /* : App.PageData */ => {
  return {
    appTitle: title,
    commit,
    customization,
    dbnomics,
    editorUrl,
    exportCsv,
    exportJson,
    exportXlsx,
    parametersRepository,
    unitByName,
    units,
  }
}
