import "$lib/i18n" // Import to initialize. Important :)
import { locale, waitLocale } from "svelte-i18n"

import { browser } from "$app/environment"
import { defaultLanguageCode, type LanguageCode } from "$lib/i18n"

import type { LayoutLoad } from "./$types"

export const load: LayoutLoad = async ({ params }) => {
  const languageCode =
    (params.languageCode as LanguageCode | undefined) ?? defaultLanguageCode
  if (browser) {
    locale.set(languageCode)
    await waitLocale()
  }
  return {
    languageCode,
  }
}
