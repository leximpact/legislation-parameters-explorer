import { redirect } from "@sveltejs/kit"

import {
  allLanguageCodes,
  defaultLanguageCode,
  type LanguageCode,
} from "$lib/i18n"
import { newParametersUrlString } from "$lib/urls"

import type { EntryGenerator, PageServerLoad } from "./$types"

export const entries = (() =>
  allLanguageCodes.map((languageCode) => ({
    languageCode:
      languageCode === defaultLanguageCode ? undefined : languageCode,
  }))) satisfies EntryGenerator

export const load: PageServerLoad = ({ params }) => {
  redirect(
    301, // Moved Permanently
    newParametersUrlString({
      languageCode:
        (params.languageCode as LanguageCode | undefined) ??
        defaultLanguageCode,
    }),
  )
}
