import { error } from "@sveltejs/kit"

import { tableOfContents } from "$lib/server/table_of_contents"

import type { LayoutServerLoad } from "./$types"
import type { TableOfContentsItem } from "$lib/table_of_contents"

export const load: LayoutServerLoad = async ({ params, parent }) => {
  const { tableOfContentsItemId } = params
  const { languageCode } = await parent()

  let tableOfContentsItem: TableOfContentsItem | undefined = undefined
  if (tableOfContentsItemId !== undefined) {
    tableOfContentsItem =
      tableOfContents?.tableOfContentsItemByIdByLanguageCode[languageCode]?.[
        tableOfContentsItemId
      ]
    if (tableOfContentsItem === undefined) {
      error(404, `Table of contents "${tableOfContentsItemId}" not found`)
    }
  }

  return {
    tableOfContentsItem,
    tableOfContentsItemId,
    ...(tableOfContents ?? {}),
  }
}
