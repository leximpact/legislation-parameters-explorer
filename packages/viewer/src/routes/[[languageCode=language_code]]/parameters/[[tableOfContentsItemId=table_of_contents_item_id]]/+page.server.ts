import {
  allLanguageCodes,
  defaultLanguageCode,
  type LanguageCode,
} from "$lib/i18n"
import { rootParameter } from "$lib/server/parameters"
import { tableOfContents } from "$lib/server/table_of_contents"

import type { EntryGenerator, PageServerLoad } from "./$types"

export const entries = (() =>
  tableOfContents === undefined
    ? []
    : (
        [] as Array<{
          languageCode?: LanguageCode
          tableOfContentsItemId: string
        }>
      ).concat.apply(
        [],
        allLanguageCodes.map((languageCode) =>
          Object.keys(
            tableOfContents!.tableOfContentsItemByIdByLanguageCode[
              languageCode
            ],
          ).map((tableOfContentsItemId) => ({
            languageCode:
              languageCode === defaultLanguageCode ? undefined : languageCode,
            tableOfContentsItemId,
          })),
        ),
      )) satisfies EntryGenerator

export const load: PageServerLoad = async () => {
  return {
    rootParameter,
  }
}
