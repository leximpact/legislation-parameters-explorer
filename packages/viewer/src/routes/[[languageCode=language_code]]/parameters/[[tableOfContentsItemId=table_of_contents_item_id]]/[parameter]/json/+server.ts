import { error, json } from "@sveltejs/kit"
import {
  convertEditableParameterToRaw,
  ParameterClass,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"

import { getEmbeddedParameter } from "$lib/parameters"
import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"

import type { RequestHandler } from "./$types"

const { exportJson } = config

export const prerender = exportJson

function cleanParameter(parameter: Parameter): Parameter {
  parameter = { ...parameter }
  delete parameter.parent
  switch (parameter.class) {
    case ParameterClass.Node:
      if (parameter.children !== undefined) {
        parameter.children = Object.fromEntries(
          Object.entries(parameter.children).map(([name, child]) => [
            name,
            cleanParameter(child),
          ]),
        )
      }
      break
    default:
  }
  return parameter
}

export const GET: RequestHandler = async ({ params }) => {
  if (!exportJson) {
    error(404, "JSON export is disabled")
  }

  const { parameter: name } = params
  const { embeddedParameter } = getEmbeddedParameter(rootParameter, name)

  if (embeddedParameter === undefined) {
    error(404, `Parameter ${name ?? "racine"} not found`)
  }

  return json(convertEditableParameterToRaw(cleanParameter(embeddedParameter)))
}
