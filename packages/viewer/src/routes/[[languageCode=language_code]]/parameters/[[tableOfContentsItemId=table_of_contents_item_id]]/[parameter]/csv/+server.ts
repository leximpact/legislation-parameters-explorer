import { error, text } from "@sveltejs/kit"
import type {
  MaybeBooleanValue,
  NumberValue,
  Reference,
} from "@tax-benefit/openfisca-json-model"
import Papa from "papaparse"
import { format, locale, unwrapFunctionStore, waitLocale } from "svelte-i18n"

import { formatCellNumber } from "$lib/formatters"
import { defaultLanguageCode, type LanguageCode } from "$lib/i18n"
import {
  countParameterColumnsApproximation,
  getEmbeddedParameter,
} from "$lib/parameters"
import {
  buildParameterColumns,
  walkParameterColumns,
  walkToTableOptions,
} from "$lib/parameters_tables"
import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"
import {
  hideParameterAncestorsMissingFromTableOfContents,
  tableOfContents,
} from "$lib/server/table_of_contents"
import { unitByName } from "$lib/server/units"
import type { TableOfContentsItem } from "$lib/table_of_contents"

import type { RequestHandler } from "./$types"

const { editorUrl, exportCsv } = config

export const prerender = exportCsv

export const GET: RequestHandler = async ({ params }) => {
  if (!exportCsv) {
    error(404, "CSV export is disabled")
  }

  const { parameter: name, tableOfContentsItemId } = params

  const languageCode =
    (params.languageCode as LanguageCode | undefined) ?? defaultLanguageCode
  locale.set(languageCode)
  await waitLocale()

  let tableOfContentsItem: TableOfContentsItem | undefined = undefined
  if (tableOfContentsItemId !== undefined) {
    tableOfContentsItem =
      tableOfContents?.tableOfContentsItemByIdByLanguageCode[languageCode]?.[
        tableOfContentsItemId
      ]
    if (tableOfContentsItem === undefined) {
      error(404, `Table of contents "${tableOfContentsItemId}" not found`)
    }
  }

  const { ancestors, embeddedParameter } = getEmbeddedParameter(
    rootParameter,
    name,
  )

  if (embeddedParameter === undefined) {
    error(404, `Parameter ${name ?? "racine"} not found`)
  }

  if (countParameterColumnsApproximation(embeddedParameter) >= 200) {
    error(404, "This parameter is too big to be exported as CSV")
  }

  const { hiddenAncestors, visibleAncestors } =
    hideParameterAncestorsMissingFromTableOfContents({
      ancestors,
      languageCode,
    })

  const tableOptions =
    tableOfContentsItem === undefined
      ? undefined
      : walkToTableOptions(
          tableOfContentsItem,
          embeddedParameter.name as string,
        )

  const { columns, dates } = buildParameterColumns({
    editorUrl,
    exclude: tableOptions?.exclude ?? [],
    hiddenAncestors,
    incomeTaxYear: tableOptions?.income_tax_year,
    languageCode,
    parameter: embeddedParameter,
    unitByName,
    visibleAncestors,
  })

  const leafColumns = [...walkParameterColumns(columns)]
    .map(({ column }) => column)
    .filter((column) => column.id !== undefined)

  const $_ = unwrapFunctionStore(format)
  const dateFormatter = new Intl.DateTimeFormat(languageCode, {
    dateStyle: "short",
  })
  const parameterNameStartRegExp = new RegExp(
    embeddedParameter.name
      ? String.raw`^${embeddedParameter.name.replace(/\./g, String.raw`\.`)}\.`
      : "^",
  )
  const labels = leafColumns.map(({ id }) =>
    id!.replace(parameterNameStartRegExp, ""),
  )
  const valueByLabelArray: Array<{ [label: string]: string }> = dates.map(
    (date) =>
      Object.fromEntries(
        leafColumns.map((column, index) => {
          const valueAtInstant = column.accessor?.(date)
          return [
            labels[index],
            valueAtInstant == null
              ? ""
              : column.id === "date"
                ? dateFormatter.format(new Date(valueAtInstant as string))
                : ["notes", "reference"].includes(column.id ?? "")
                  ? (valueAtInstant as Reference[])
                      .map((reference) =>
                        [reference.title, reference.href, reference.note]
                          .filter((item) => item !== undefined)
                          .join("; "),
                      )
                      .join(" | ")
                  : column.id === "official_journal_date"
                    ? (valueAtInstant as string[]).join(" | ")
                    : column.id === "raw"
                      ? (valueAtInstant as string)
                      : valueAtInstant === "expected"
                        ? $_("expected")
                        : valueAtInstant === "empty" ||
                            (valueAtInstant as NumberValue).value === null
                          ? ""
                          : typeof (valueAtInstant as MaybeBooleanValue)
                                .value === "boolean"
                            ? (valueAtInstant as MaybeBooleanValue).value
                              ? "true"
                              : "false"
                            : typeof (valueAtInstant as NumberValue).value ===
                                "number"
                              ? formatCellNumber(
                                  (valueAtInstant as NumberValue).value,
                                  unitByName,
                                  column.unit,
                                  date,
                                  languageCode,
                                )
                              : JSON.stringify(valueAtInstant), // TODO
          ]
        }),
      ),
  )

  return text(
    Papa.unparse(valueByLabelArray, { columns: labels, delimiter: "," }),
    {
      headers: {
        "Content-Type": "text/csv; charset=utf-8",
        "Content-Disposition": `attachment; filename="${name}.csv"`,
      },
    },
  )
}
