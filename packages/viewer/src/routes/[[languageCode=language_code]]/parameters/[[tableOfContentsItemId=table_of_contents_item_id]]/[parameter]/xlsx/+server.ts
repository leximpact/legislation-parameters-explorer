import { error } from "@sveltejs/kit"
import type {
  MaybeBooleanValue,
  NumberValue,
  Reference,
} from "@tax-benefit/openfisca-json-model"
import { format, locale, unwrapFunctionStore, waitLocale } from "svelte-i18n"
import ExcelJS from "exceljs"

import { defaultLanguageCode, type LanguageCode } from "$lib/i18n"
import {
  countParameterColumnsApproximation,
  getEmbeddedParameter,
} from "$lib/parameters"
import {
  buildParameterColumns,
  computeMaxDepth,
  computeRowspan,
  iterRange,
  walkParameterColumns,
  walkToTableOptions,
} from "$lib/parameters_tables"
import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"
import {
  hideParameterAncestorsMissingFromTableOfContents,
  tableOfContents,
} from "$lib/server/table_of_contents"
import { unitByName } from "$lib/server/units"
import type { TableOfContentsItem } from "$lib/table_of_contents"

import type { RequestHandler } from "./$types"
import { formatCellNumber } from "$lib/formatters"

const { editorUrl, exportXlsx } = config

export const prerender = exportXlsx

export const GET: RequestHandler = async ({ params }) => {
  if (!exportXlsx) {
    error(404, "XLSX export is disabled")
  }
  const { parameter: name, tableOfContentsItemId } = params

  const languageCode =
    (params.languageCode as LanguageCode | undefined) ?? defaultLanguageCode
  locale.set(languageCode)
  await waitLocale()

  let tableOfContentsItem: TableOfContentsItem | undefined = undefined
  if (tableOfContentsItemId !== undefined) {
    tableOfContentsItem =
      tableOfContents?.tableOfContentsItemByIdByLanguageCode[languageCode]?.[
        tableOfContentsItemId
      ]
    if (tableOfContentsItem === undefined) {
      error(404, `Table of contents "${tableOfContentsItemId}" not found`)
    }
  }

  const { ancestors, embeddedParameter } = getEmbeddedParameter(
    rootParameter,
    name,
  )

  if (embeddedParameter === undefined) {
    error(404, `Parameter ${name ?? "racine"} not found`)
  }

  if (countParameterColumnsApproximation(embeddedParameter) >= 200) {
    error(404, "This parameter is too big to be exported as XLSX")
  }

  const { hiddenAncestors, visibleAncestors } =
    hideParameterAncestorsMissingFromTableOfContents({
      ancestors,
      languageCode,
    })

  const tableOptions =
    tableOfContentsItem === undefined
      ? undefined
      : walkToTableOptions(
          tableOfContentsItem,
          embeddedParameter.name as string,
        )

  const { columns, dates, documentation } = buildParameterColumns({
    editorUrl,
    exclude: tableOptions?.exclude ?? [],
    hiddenAncestors,
    incomeTaxYear: tableOptions?.income_tax_year,
    languageCode,
    parameter: embeddedParameter,
    unitByName,
    visibleAncestors,
  })

  const $_ = unwrapFunctionStore(format)
  const dateFormatter = new Intl.DateTimeFormat(languageCode, {
    dateStyle: "short",
  })
  const leafColumns = [...walkParameterColumns(columns)]
    .map(({ column }) => column)
    .filter((column) => column.id !== undefined)

  const maxDepth = computeMaxDepth(columns)
  const workbook = new ExcelJS.Workbook()
  let worksheetName = name.split(".").at(-1) as string
  if (worksheetName.length > 30) {
    worksheetName = worksheetName.slice(0, 29) + "…"
  }
  const sheet = workbook.addWorksheet(worksheetName)
  sheet.columns = columns.map((column) => ({
    key: column.id,
    width: (column.width ?? 1) * 20,
  }))

  for (const rowIndex of iterRange(0, maxDepth)) {
    let columnIndex = 0
    for (const { column, depth } of walkParameterColumns(columns)) {
      if (depth === rowIndex + 1) {
        const { colspan, header } = column
        columnIndex = getFreeCellColumnIndex(sheet, rowIndex, columnIndex)
        const cell = getCell(sheet, rowIndex, columnIndex)
        cell.value = header.shortTitle
        cell.alignment = {
          horizontal: "center",
          vertical: "middle",
          wrapText: true,
        }
        sheet.mergeCells(
          rowIndex + 1,
          columnIndex + 1,
          rowIndex + computeRowspan(column, depth, maxDepth),
          columnIndex + (colspan ?? 1),
        )
        columnIndex += colspan ?? 1
      }
    }
  }

  for (const date of dates) {
    sheet.addRow(
      leafColumns.map((column) => {
        const valueAtInstant = column.accessor?.(date)
        return valueAtInstant == null
          ? ""
          : column.id === "date"
            ? dateFormatter.format(new Date(valueAtInstant as string))
            : ["notes", "reference"].includes(column.id ?? "")
              ? (valueAtInstant as Reference[]).length === 1 &&
                (valueAtInstant as Reference[])[0].href !== undefined
                ? {
                    hyperlink: (valueAtInstant as Reference[])[0].href,
                    text:
                      (valueAtInstant as Reference[])[0].title ??
                      (valueAtInstant as Reference[])[0].href,
                    tooltip: (valueAtInstant as Reference[])[0].note,
                  }
                : (valueAtInstant as Reference[])
                    .map(({ href, note, title }) =>
                      [title, href, note]
                        .filter((item) => item !== undefined)
                        .join("; "),
                    )
                    .join(" | ")
              : column.id === "official_journal_date"
                ? (valueAtInstant as string[]).join(" | ")
                : column.id === "raw"
                  ? (valueAtInstant as string)
                  : valueAtInstant === "expected"
                    ? $_("expected")
                    : valueAtInstant === "empty" ||
                        (valueAtInstant as NumberValue).value === null
                      ? ""
                      : typeof (valueAtInstant as MaybeBooleanValue).value ===
                          "boolean"
                        ? ((valueAtInstant as MaybeBooleanValue).value ?? false)
                        : typeof (valueAtInstant as NumberValue).value ===
                            "number"
                          ? formatCellNumber(
                              (valueAtInstant as NumberValue).value,
                              unitByName,
                              column.unit,
                              date,
                              languageCode,
                            )
                          : JSON.stringify(valueAtInstant) // TODO
      }),
    )
  }

  if (documentation !== undefined) {
    sheet.addRow([])
    sheet.addRow([documentation])
  }

  return new Response(await workbook.xlsx.writeBuffer(), {
    headers: {
      "Content-Type":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "Content-Disposition": `attachment; filename="${name}.xlsx"`,
    },
    status: 200,
  })
}

function getCell(
  sheet: ExcelJS.Worksheet,
  rowIndex: number,
  columnIndex: number,
): ExcelJS.Cell {
  return sheet.getRow(rowIndex + 1).getCell(columnIndex + 1)
}

function getFreeCellColumnIndex(
  sheet: ExcelJS.Worksheet,
  rowIndex: number,
  minColumnIndex: number,
): number {
  const cell = getCell(sheet, rowIndex, minColumnIndex)
  return isMerged(cell)
    ? getFreeCellColumnIndex(sheet, rowIndex, minColumnIndex + 1)
    : minColumnIndex
}

function isMerged(cell: ExcelJS.Cell): boolean {
  return cell.master !== cell
}
