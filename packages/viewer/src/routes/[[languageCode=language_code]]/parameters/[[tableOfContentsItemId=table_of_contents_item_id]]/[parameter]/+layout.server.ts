import { error } from "@sveltejs/kit"

import { getEmbeddedParameter } from "$lib/parameters"
import { rootParameter } from "$lib/server/parameters"

import type { LayoutServerLoad } from "./$types"
import { hideParameterAncestorsMissingFromTableOfContents } from "$lib/server/table_of_contents"

export const load: LayoutServerLoad = async ({ params, parent }) => {
  const { parameter: name } = params
  const { languageCode } = await parent()

  const { ancestors, embeddedParameter } = getEmbeddedParameter(
    rootParameter,
    name,
  )
  if (embeddedParameter === undefined) {
    error(404, `Parameter "${name}" not found`)
  }

  const { hiddenAncestors, visibleAncestors } =
    hideParameterAncestorsMissingFromTableOfContents({
      ancestors,
      languageCode,
    })

  return {
    ancestors,
    embeddedParameter,
    fileParameter: undefined,
    hiddenAncestors,
    visibleAncestors,
  }
}
