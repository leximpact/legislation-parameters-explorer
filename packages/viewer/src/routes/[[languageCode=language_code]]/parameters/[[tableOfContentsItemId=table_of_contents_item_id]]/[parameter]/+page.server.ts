import {
  allLanguageCodes,
  defaultLanguageCode,
  type LanguageCode,
} from "$lib/i18n"
import { rootParameter } from "$lib/server/parameters"
import { tableOfContents } from "$lib/server/table_of_contents"
import { walkTableOfContentsItemParametersName } from "$lib/server/table_of_contents_without_data"
import { walkVisibleParameters } from "$lib/server/visible_parameters"

import type { EntryGenerator } from "./$types"

export const entries = (() => {
  const entries: Array<{
    languageCode?: LanguageCode
    parameter: string
    tableOfContentsItemId?: string
  }> = []
  if (tableOfContents === undefined) {
    for (const languageCode of allLanguageCodes) {
      for (const parameter of walkVisibleParameters(
        rootParameter,
        languageCode,
      )) {
        if (parameter.name) {
          entries.push({
            languageCode:
              languageCode === defaultLanguageCode ? undefined : languageCode,
            parameter: parameter.name,
          })
        }
      }
    }
  } else {
    for (const languageCode of allLanguageCodes) {
      for (const [tableOfContentsItemId, tableOfContentsItem] of Object.entries(
        tableOfContents.tableOfContentsItemByIdByLanguageCode[languageCode],
      )) {
        for (const parameterName of walkTableOfContentsItemParametersName(
          tableOfContentsItem,
        )) {
          if (parameterName) {
            entries.push({
              languageCode:
                languageCode === defaultLanguageCode ? undefined : languageCode,
              parameter: parameterName,
              tableOfContentsItemId,
            })
          }
        }
      }
    }
  }
  return entries
}) satisfies EntryGenerator
