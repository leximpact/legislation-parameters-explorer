import { error } from "@sveltejs/kit"

import { countParameterColumnsApproximation } from "$lib/parameters"

import type { PageServerLoad } from "./$types"
import { walkToTableOptions } from "$lib/parameters_tables"

export const load: PageServerLoad = async ({ parent }) => {
  const { embeddedParameter, tableOfContentsItem } = await parent()

  if (countParameterColumnsApproximation(embeddedParameter) >= 200) {
    error(404, "This parameter can't be displayed as a table")
  }

  return {
    tableOptions:
      tableOfContentsItem === undefined
        ? undefined
        : walkToTableOptions(
            tableOfContentsItem,
            embeddedParameter.name as string,
          ),
  }
}
