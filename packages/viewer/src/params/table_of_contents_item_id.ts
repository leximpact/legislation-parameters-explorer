import type { ParamMatcher } from "@sveltejs/kit"

import { tableOfContentsItemsIdByLanguageCode } from "$lib/data/table_of_contents.json"

export const match: ParamMatcher = (param) =>
  Object.values(
    tableOfContentsItemsIdByLanguageCode as {
      [languageCode: string]: string[]
    },
  ).some((tableOfContentsItemsId) => tableOfContentsItemsId.includes(param))
